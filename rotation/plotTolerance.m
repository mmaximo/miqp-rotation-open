function plotTolerance(amplitudes, directions, resultsMatrix)
    imagesc(directions, amplitudes, ~resultsMatrix);
    colors = linspecer(2);
    colormap(colors);
    grid on;
    xlabel('Direction ($^{\circ}$)', 'FontSize', 14, 'interpreter', 'latex');
    ylabel('Amplitude ($m/s$)', 'FontSize', 14, 'interpreter', 'latex');
    set(gca, 'FontSize', 14);
    set(gca, 'TickLabelInterpreter','latex')
end