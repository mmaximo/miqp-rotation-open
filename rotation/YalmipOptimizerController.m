classdef YalmipOptimizerController < handle

    properties
        type;
        T;
        zCoM;
        N;
        Nf;
        Nr;
        Nstep;
        maxVelocity;
        dthetaMax;
        maxAcceleration;
        footDimensions;
        ySep;
        alpha;
        beta;
        gamma;
        delta;
        xi;
        switchingStep;
        processingTimes;
        lastSolution;
        hasLastSolution;
        problem;
        optVariables;
    end
    
    methods
        function self = YalmipOptimizerController(T, zCoM, N, Nf, Nr, Nstep,...
                vxmax, vymax, axmax, aymax, footDimensions, ySep,...
                alpha, beta, gamma, delta, xi, dthetaMax)
            self.type = 'optimizer';
            self.T = T;
            self.zCoM = zCoM;
            self.maxVelocity.vxmax = vxmax;
            self.maxVelocity.vymax = vymax;
            self.maxAcceleration.axmax = axmax;
            self.maxAcceleration.aymax = aymax;
            self.footDimensions = footDimensions;
            self.ySep = ySep;
            self.N = N;
            self.Nf = Nf;
            self.Nr = Nr;
            self.Nstep = Nstep;
            self.alpha = alpha;
            self.beta = beta;
            self.gamma = gamma;
            self.delta = delta;
            self.xi = xi;
            self.dthetaMax = dthetaMax;
            self.processingTimes = [];
            self.lastSolution = struct;
            self.hasLastSolution = false;
            self.problem = self.makeOptimizationProblem();
        end
        
        function problem = makeOptimizationProblem(self)
            variables = YalmipOptimizerFactory.makeVariables(self.N, self.Nf, self.Nr);
            
            variablesCell = YalmipOptimizerFactory.makeVariablesCell(variables);
            
            params = YalmipOptimizerFactory.makeParams(self.N, self.Nr);
            
            paramsCell = YalmipOptimizerFactory.makeParamsCell(params);
            
            [dxsp, dysp, bsp] = YalmipOptimizerFactory.makeSupportPolygon(params, self.footDimensions.length, self.footDimensions.width);
            [dxfr, dyfr, bfr] = YalmipOptimizerFactory.makeFootReachability(params, self.ySep);
            [dxvl, dyvl, bvl] = YalmipOptimizerFactory.makeVelocityLimits(params, self.maxVelocity.vxmax, self.maxVelocity.vymax);
            [dxa, dya, ba] = YalmipOptimizerFactory.makeAccelerationLimits(params, self.maxAcceleration.axmax, self.maxAcceleration.aymax);
            
            prediction = Prediction(LIPMDynamics(self.T, self.zCoM), self.N);
            
            jerkCost = YalmipOptimizerFactory.makeJerkCost(variables, self.alpha);
            velTrackCost = YalmipOptimizerFactory.makeVelocityTrackingCost(variables, params, prediction, self.beta);
            zmpTrackCost = YalmipOptimizerFactory.makeZMPTrackingCost(variables, params, prediction, self.gamma);
            stepDurCost = YalmipOptimizerFactory.makeStepDurationCost(variables, self.delta);
            rotationCost = YalmipOptimizerFactory.makeRotationCost(variables, params, self.xi);
            
            cost = jerkCost + velTrackCost + zmpTrackCost + stepDurCost + rotationCost;
            
            integerConst = YalmipOptimizerFactory.makeIntegerConstraints(variables);
            auxVarConst = YalmipOptimizerFactory.makeAuxiliaryVariablesConstraints(variables);
            zmpErrorConst = YalmipOptimizerFactory.makeZMPErrorConstraints(variables, params, prediction,...
                dxsp, dysp, bsp);
            feetCollConst = YalmipOptimizerFactory.makeFeetCollisionConstraints(variables, params,...
                dxfr, dyfr, bfr);
            jointsConst = YalmipOptimizerFactory.makeJointsConstraints(variables, params,...
                dxvl, dyvl, bvl, self.T, self.dthetaMax);
            currStepConst = YalmipOptimizerFactory.makeCurrentStepConstraints(variables, params);
            maxAccelConst = YalmipOptimizerFactory.makeAccelerationConstraints(variables, params,...
                prediction, dxa, dya, ba);
            stepDurRefContConst = YalmipOptimizerFactory.makeStepDurationReferenceContinuousConstraints(variables, params,...
                self.Nstep);
            stepDurRefBinConst = YalmipOptimizerFactory.makeStepDurationReferenceBinaryConstraints(variables, params,...
                self.Nstep);
            footRotationConst = YalmipOptimizerFactory.makeFootRotationConstraints(variables, params, self.Nstep, self.T);
            boundConst = YalmipOptimizerFactory.makeBoundConstraints(variables, params);
            
            constraints = integerConst + auxVarConst + zmpErrorConst + feetCollConst +...
                jointsConst + currStepConst + maxAccelConst + stepDurRefContConst +...
                stepDurRefBinConst + footRotationConst + boundConst;
            
            options = sdpsettings('solver', 'gurobi', 'verbose', 0);
%             options = sdpsettings('solver', 'gurobi', 'verbose', 0, 'verbose', 2);
%             options = sdpsettings('solver', 'gurobi', 'gurobi.NumericFocus', 3, 'verbose', 0);
            
%             self.optVariables = variables;

            self.optVariables = variables;
            
            problem = optimizer(constraints, cost, options, paramsCell, variablesCell);
        end
        
        function [d3X, d3Y, Xfb, Yfb, Xfaux, Yfaux, Thetafb, Thetafaux, Bu, feasible] = control(self, k, x, y,...
                dXr, dYr, dthetar, xc, yc, xa, ya, thetaa, theta, isLeftSwing)
            kprime = k;
%             if k > 5
%                 kprime = 5;
%             end
            bds = 0;
            if k == 0
                bds = 1;
            end
            
            bccw = 1;
            if dthetar < 0
                bccw = 0;
            end
            
            if isLeftSwing
                bls = 1;
            else
                bls = 0;
            end
            
            % This is hacky...
            thetac = theta(1);
            if abs(dthetar) > 1e-3
                Thetafp = thetac + (0:self.Nr-1)' * self.T * dthetar;
            else
                Thetafp = thetac + (0:self.Nr-1)' * self.T * 0.1;
            end
            cfp = cos(Thetafp);
            sfp = sin(Thetafp);
            
            paramsValues = {
                dXr,...
                dYr,...
                dthetar,...
                kprime,...
                x,...
                y,...
                xc,...
                yc,...
                thetac,...
                xa,...
                ya,...
                thetaa,...
                bds,...
                bls,...
                bccw,...
                Thetafp,...
                cfp,...
                sfp
                };
            
%             if self.hasLastSolution
%                 assign(self.optVariables.Bs(1), 1);
%                 if self.lastSolution.stepChanging
%                     assign(self.optVariables.Ub(1:end-1, 1:end-1), self.lastSolution.Ub(2:end, 2:end));
%                     assign(self.optVariables.Thetafb(1:end-1, :), self.lastSolution.Thetafb(2:end, :));
%                     assign(self.optVariables.Thetafaux(1:end-1, :), self.lastSolution.Thetafaux(2:end, :));
%                 else
%                     assign(self.optVariables.Ub(1:end-1, 1:end), self.lastSolution.Ub(2:end, 1:end));
%                     assign(self.optVariables.Thetafb(1:end, :), self.lastSolution.Thetafb(1:end, :));
%                     assign(self.optVariables.Thetafaux(1:end-1, :), self.lastSolution.Thetafaux(2:end, :));
%                 end
%             else
%                 assign(self.optVariables.Bs(1), 1);
%                 assign(self.optVariables.Ub, [ones(self.Nstep, 1), zeros(self.Nstep, 1), zeros(self.Nstep, 1); 
%                                       zeros(self.Nstep, 1), ones(self.Nstep, 1), zeros(self.Nstep, 1)]);
%                 assign(self.optVariables.Thetafb, [ones(self.Nf, 1), zeros(self.Nf, self.Nr-1)]);
%                 assign(self.optVariables.Thetafaux, [ones(self.N, 1), zeros(self.N, self.Nr-1)]);
%             end
            
            tic;
            [solution, error] = self.problem(paramsValues);
            self.processingTimes(end+1) = toc;
%             self.processingTimes(end+1) = diagnostics.solvertime;
            

%             variables.d3X,...       % 1
%             variables.Xfb,...       % 2
%             variables.d3Y,...       % 3
%             variables.Yfb,...       % 4
%             variables.Xfaux,...     % 5
%             variables.Yfaux,...     % 6
%             variables.Sr,...        % 7
%             variables.Bu,...        % 8
%             variables.Ub,...        % 9
%             variables.Bs,...        % 10
%             variables.Thetafb,...   % 11
%             variables.Thetafaux,... % 12
%             variables.Thetar,...    % 13
%             variables.deltaThetar   % 14

            d3X = solution{1};%value(variables.d3X);
            d3Y = solution{3};%value(variables.d3Y);
            Xfb = solution{2};%value(variables.Xfb);
            Yfb = solution{4};%value(variables.Yfb);
            Xfaux = solution{5};%value(variables.Xfaux);
            Yfaux = solution{6};%value(variables.Yfaux);
            Thetafb = solution{11};%value(variables.Thetafb);
            Thetafaux = solution{12};%value(variables.Thetafaux);
            Bu = solution{8};%value(variables.Bu);
            Ub = solution{9};
            Bs = solution{10};
            Sr = solution{7};
%             jerkCost = YalmipOptimizerFactory.makeJerkCost(self.optVariables, self.alpha);
%             velTrackCost = YalmipOptimizerFactory.makeVelocityTrackingCost(variables, params, prediction, self.beta);
%             zmpTrackCost = YalmipOptimizerFactory.makeZMPTrackingCost(variables, params, prediction, self.gamma);
%             stepDurCost = YalmipOptimizerFactory.makeStepDurationCost(self.optVariables, self.delta);
%             stepDurCost
%             rotationCost = YalmipOptimizerFactory.makeRotationCost(variables, params, self.xi);
%             value(jerkCost)
%             value(stepDurCost)
            
%             Bs
%             Ub
%             (1:self.Nf) * Ub(self.N, :)'
%             Sr
            self.hasLastSolution = true;
            self.lastSolution = struct;
            self.lastSolution.Ub = solution{9};%value(variables.Ub);
            self.lastSolution.Bs = solution{10};%value(variables.Bs);
            self.lastSolution.Thetafb = solution{11};%value(variables.Thetafb);
            self.lastSolution.Thetafaux = solution{12};%value(variables.Thetafaux);
            self.lastSolution.stepChanging = (abs(sum(self.lastSolution.Ub(:, 1)) - 1) <= 1e-3);
            
            feasible = (error == 0);
        end
        
        function numSteps = getNumSteps(self, k)
            numSteps = self.N;
        end
    end

end