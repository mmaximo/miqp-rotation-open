classdef YalmipRotationTester < handle
    methods(Static)
        function testStepDurationReferenceConstraints()
            N = 10;
            Nf = 3;
            delta = 100;
            nref = 5;
            kprime = 3;
            
            Nbu = Nf * (Nf + 1) / 2 + (N - Nf) * Nf;
            variables.N = N;
            variables.Nf = Nf;
            variables.Nbu = Nbu;
            variables.Sr = sdpvar(Nf, 1);
            variables.Bu = binvar(Nbu, 1);
            variables.Ub = YalmipRotationOptimizationFactory.makeUb(variables.Bu, N, Nf);
            variables.Bs = binvar(Nf, 1);
            
            stepDurCost = YalmipRotationOptimizationFactory.makeStepDurationCost(variables, delta);
            
            integerConst = YalmipRotationOptimizationFactory.makeIntegerConstraints(variables);
            stepDurRefContConst = YalmipRotationOptimizationFactory.makeStepDurationReferenceContinuousConstraints(variables, nref, kprime);
            stepDurRefBinConst = YalmipRotationOptimizationFactory.makeStepDurationReferenceBinaryConstraints(variables, nref, kprime);
            
            constraints = integerConst + stepDurRefContConst + stepDurRefBinConst;
            
            optimize(constraints, stepDurCost);
            
            value(variables.Sr)
            YalmipTester.binaryVectorToMatrix(variables.Bu, N, Nf)
            variables.Ub
            value(variables.Ub)
            value(variables.Bs)
%             Tester.binaryVectorToMatrix(variables(2).values, N, Nf)
%             variables(3).values
%             
%             ctype = VariableFactory.getVariablesTypesVector(variables);
%             
%             stepDurCost = CostFactory.makeStepDurationCost(N, Nf, delta);
%             
%             [QStepDur, pStepDur] = CostFactory.makeCostMatrices(variables, stepDurCost);
%             
%             Q = QStepDur;
%             Q = (Q + Q') / 2;
%             p = pStepDur;
%             
%             integerConst = ConstraintFactory.makeIntegerConstraints(N, Nf);
%             stepDurRefContConst = ConstraintFactory.makeStepDurationReferenceContinuousConstraints(nref, kprime, M, N, Nf);
%             stepDurRefBinConst = ConstraintFactory.makeStepDurationReferenceBinaryConstraints(nref, kprime, N, Nf);
%             
%             [AineqInteger, bineqInteger] = ConstraintFactory.makeConstraintsMatrices(variables, integerConst);
%             [AineqStepDurRefCont, bineqStepDurRefCont] = ConstraintFactory.makeConstraintsMatrices(variables, stepDurRefContConst);
%             [AineqStepDurRefBin, bineqStepDurRefBin] = ConstraintFactory.makeConstraintsMatrices(variables, stepDurRefBinConst);
%             
%             Aineq = [AineqInteger; AineqStepDurRefCont; AineqStepDurRefBin];
%             bineq = [bineqInteger; bineqStepDurRefCont; bineqStepDurRefBin];
%             
%             u = gurobiMiqp(Q, p, Aineq, bineq, [], [], [], [], [], [], [], ctype, [], []);
%             
%             variables = VariableFactory.extractValues(u, variables);
%             
%             variables(1).values
%             Tester.binaryVectorToMatrix(variables(2).values, N, Nf)
%             variables(3).values
        end
        
        function testJerkCost()
            alpha = 10^-6;
            beta = 1;
            N = 10;
            Nf = 3;
            Nr = 10;
            nref = 5;
            kprime = 0;
            M = 10^3;
            delta = 100;
            T = 0.1;
            zCOM = 0.2;
            footDimensions = struct;
            footDimensions.length = 0.6;
            footDimensions.width = 0.3;
            
            prediction = Prediction(LIPMDynamics(T, zCOM), N);
            
            x = [0; 0; 0];
            y = [0; 0; 0];
            dXref = ones(N, 1);
            dYref = ones(N, 1);
            
            variables = YalmipRotationOptimizationFactory.makeVariables(N, Nf, Nr);
            
            jerkCost = YalmipRotationOptimizationFactory.makeJerkCost(variables, alpha);
            velTrackCost = YalmipRotationOptimizationFactory.makeVelocityTrackingCost(variables, prediction, dXref, dYref, x, y, beta);
            stepDurCost = YalmipRotationOptimizationFactory.makeStepDurationCost(variables, delta);
            
            cost = jerkCost + velTrackCost + stepDurCost;
            
            integerConst = YalmipRotationOptimizationFactory.makeIntegerConstraints(variables);
            stepDurRefContConst = YalmipRotationOptimizationFactory.makeStepDurationReferenceContinuousConstraints(variables, nref, kprime);
            stepDurRefBinConst = YalmipRotationOptimizationFactory.makeStepDurationReferenceBinaryConstraints(variables, nref, kprime);
            
            constraints = integerConst + stepDurRefContConst + stepDurRefBinConst;
            
            optimize(constraints, cost);
            
            d3X = value(variables.d3X);
            d3Y = value(variables.d3Y);
            Xfb = value(variables.Xfb);
            Yfb = value(variables.Yfb);
            Xfaux = value(variables.Xfaux);
            Yfaux = value(variables.Yfaux);
            
            theta = zeros(N, 1);
            
            PlotterHumanoids.plotTest(d3X, d3Y, Xfb, Yfb, Xfaux, Yfaux,...
                theta, prediction, x, y, T, N, footDimensions);
        end
        
        function testZMPSupportPolygon()
            alpha = 10^-6;
            beta = 1;
            gamma = 10^-6;
            N = 10;
            Nf = 3;
            Nr = 10;
            nref = 5;
            kprime = 0;
            M = 10^3;
            delta = 100;
            xi = 100;
            T = 0.1;
            zCOM = 0.2;
            footDimensions = struct;
            footDimensions.length = 0.6;
            footDimensions.width = 0.3;
            xc = 0;
            yc = 0;
            
            theta = zeros(Nr, 1);
            
            prediction = Prediction(LIPMDynamics(T, zCOM), N);
            
            x = [0; 0; 0];
            y = [0; 0; 0];
            dXref = ones(N, 1);
            dYref = ones(N, 1);
            
            [dxsp, dysp, bsp] = YalmipRotationOptimizationFactory.makeSupportPolygon(footDimensions, theta);
            
            variables = YalmipRotationOptimizationFactory.makeVariables(N, Nf, Nr);
            
            jerkCost = YalmipRotationOptimizationFactory.makeJerkCost(variables, alpha);
            velTrackCost = YalmipRotationOptimizationFactory.makeVelocityTrackingCost(variables, prediction, dXref, dYref, x, y, beta);
            zmpTrackCost = YalmipRotationOptimizationFactory.makeZMPTrackingCost(variables, prediction, x, y, gamma);
            stepDurCost = YalmipRotationOptimizationFactory.makeStepDurationCost(variables, delta);
            rotationCost = YalmipRotationOptimizationFactory.makeRotationCost(variables, xi, 0.0, zeros(Nf, 1));
            
            cost = jerkCost + velTrackCost + zmpTrackCost + stepDurCost + rotationCost;
            
            integerConst = YalmipRotationOptimizationFactory.makeIntegerConstraints(variables);
            auxVarConst = YalmipRotationOptimizationFactory.makeAuxiliaryVariablesConstraints(variables);
            zmpErrorConst = YalmipRotationOptimizationFactory.makeZMPErrorConstraints(variables, prediction, dxsp, dysp, bsp, x, y);
            currStepConst = YalmipRotationOptimizationFactory.makeCurrentStepConstraints(variables, xc, yc);
            stepDurRefContConst = YalmipRotationOptimizationFactory.makeStepDurationReferenceContinuousConstraints(variables, nref, kprime);
            stepDurRefBinConst = YalmipRotationOptimizationFactory.makeStepDurationReferenceBinaryConstraints(variables, nref, kprime);
            footRotationConst = YalmipRotationOptimizationFactory.makeFootRotationConstraints(variables);
            boundConst = YalmipRotationOptimizationFactory.makeBoundConstraints(variables);
            
            constraints = integerConst + auxVarConst + zmpErrorConst +...
                currStepConst + stepDurRefContConst + stepDurRefBinConst +...
                footRotationConst + boundConst;
            
            optimize(constraints, cost);
            
            d3X = value(variables.d3X);
            d3Y = value(variables.d3Y);
            Xfb = value(variables.Xfb);
            Yfb = value(variables.Yfb);
            Xfaux = value(variables.Xfaux);
            Yfaux = value(variables.Yfaux);
            Thetafb = value(variables.Thetafb);
            Thetafaux = value(variables.Thetafaux);
            
            Xfb
            Xfaux
            Ub = value(variables.Ub)
            Thetafb
            Thetafaux
            variables.Thetafb
            variables.Thetafaux
            
            PlotterHumanoids.plotTest(d3X, d3Y, Xfb, Yfb, Xfaux, Yfaux,...
                theta, prediction, x, y, T, N, footDimensions);
        end
        
        function testZMPFeetCollision()
            alpha = 10^-6;
            beta = 1;
            gamma = 10^-6;
            N = 10;
            Nf = 3;
            Nr = 10;
            nref = 5;
            kprime = 0;
            M = 10^3;
            delta = 100;
            xi = 100;
            T = 0.1;
            zCOM = 0.2;
            footDimensions = struct;
            footDimensions.length = 0.6;
            footDimensions.width = 0.3;
            xc = 0;
            yc = 0;
            isLeftSwing = true;
            ySep = 0.4;
            deltaTheta = 0.0;
            
            theta = zeros(Nr, 1);
            
            prediction = Prediction(LIPMDynamics(T, zCOM), N);
            
            x = [0; 0; 0];
            y = [0; 0; 0];
            dXref = ones(N, 1);
            dYref = zeros(N, 1);
            
            variables = YalmipRotationOptimizationFactory.makeVariables(N, Nf, Nr);
            
            [dxfr, dyfr, bfr] = YalmipRotationOptimizationFactory.makeFootReachability(ySep, theta);
            [dxsp, dysp, bsp] = YalmipRotationOptimizationFactory.makeSupportPolygon(footDimensions, theta);
            
            jerkCost = YalmipRotationOptimizationFactory.makeJerkCost(variables, alpha);
            velTrackCost = YalmipRotationOptimizationFactory.makeVelocityTrackingCost(variables, prediction, dXref, dYref, x, y, beta);
            zmpTrackCost = YalmipRotationOptimizationFactory.makeZMPTrackingCost(variables, prediction, x, y, gamma);
            stepDurCost = YalmipRotationOptimizationFactory.makeStepDurationCost(variables, delta);
            rotationCost = YalmipRotationOptimizationFactory.makeRotationCost(variables, xi, 0.0, zeros(Nf, 1));
            
            cost = jerkCost + velTrackCost + zmpTrackCost + stepDurCost + rotationCost;
            
            integerConst = YalmipRotationOptimizationFactory.makeIntegerConstraints(variables);
            auxVarConst = YalmipRotationOptimizationFactory.makeAuxiliaryVariablesConstraints(variables);
            zmpErrorConst = YalmipRotationOptimizationFactory.makeZMPErrorConstraints(variables, prediction, dxsp, dysp, bsp, x, y);
            feetCollConst = YalmipRotationOptimizationFactory.makeFeetCollisionConstraints(variables, dxfr, dyfr, bfr, isLeftSwing, deltaTheta);
            currStepConst = YalmipRotationOptimizationFactory.makeCurrentStepConstraints(variables, xc, yc);
            stepDurRefContConst = YalmipRotationOptimizationFactory.makeStepDurationReferenceContinuousConstraints(variables, nref, kprime);
            stepDurRefBinConst = YalmipRotationOptimizationFactory.makeStepDurationReferenceBinaryConstraints(variables, nref, kprime);
            footRotationConst = YalmipRotationOptimizationFactory.makeFootRotationConstraints(variables);
            boundConst = YalmipRotationOptimizationFactory.makeBoundConstraints(variables);
            
            constraints = integerConst + auxVarConst + zmpErrorConst +...
                feetCollConst + currStepConst + stepDurRefContConst + stepDurRefBinConst +...
                footRotationConst + boundConst;
            
            optimize(constraints, cost);
            
            d3X = value(variables.d3X);
            d3Y = value(variables.d3Y);
            Xfb = value(variables.Xfb);
            Yfb = value(variables.Yfb);
            Xfaux = value(variables.Xfaux);
            Yfaux = value(variables.Yfaux);
            
            PlotterHumanoids.plotTest(d3X, d3Y, Xfb, Yfb, Xfaux, Yfaux,...
                theta, prediction, x, y, T, N, footDimensions);
        end
        
        function testFullMIQP()
            alpha = 10^-6;
            beta = 1;
            gamma = 10^-6;
            N = 10;
            Nf = 3;
            Nr = 10;
            nref = 5;
            kprime = 0;
%             M = 10^3;
            delta = 1000;
            xi = 1000;
            T = 0.1;
            zCOM = 0.2;
            footDimensions = struct;
            footDimensions.length = 0.6;
            footDimensions.width = 0.3;
            xc = 0;
            yc = 0;
            thetac = 0;
            isLeftSwing = true;
            ySep = 0.6;
            xa = 0;
            ya = 0;
            axmax = 10;
            aymax = 10;
            maxAcceleration.axmax = axmax;
            maxAcceleration.aymax = aymax;
            maxVelocity.vxmax = 10;
            maxVelocity.vymax = 10;
            thetaa = 0.0;
            deltaTheta = 0.05;
            deltaThetaMax = 0.2;
            
            theta = deltaTheta * (0:Nr-1)';
            Thetaref = [0.0; 0.1; 0.2];%0.1 * (0:Nf-1)';
%             theta = 0*(0.2:0.2:(Nf*0.2));
            
            prediction = Prediction(LIPMDynamics(T, zCOM), N);
            
            x = [0; 0; 0];
            y = [0; 0; 0];
            dXref = ones(N, 1);
            dYref = zeros(N, 1);
            
            variables = YalmipRotationOptimizationFactory.makeVariables(N, Nf, Nr);
            
            [dxfr, dyfr, bfr] = YalmipRotationOptimizationFactory.makeFootReachability(ySep, theta);
            [dxsp, dysp, bsp] = YalmipRotationOptimizationFactory.makeSupportPolygon(footDimensions, theta);
            [dxvl, dyvl, bvl] = YalmipRotationOptimizationFactory.makeVelocityLimits(maxVelocity, theta);
            [dxa, dya, ba] = YalmipRotationOptimizationFactory.makeAccelerationLimits(maxAcceleration, theta);
            
            jerkCost = YalmipRotationOptimizationFactory.makeJerkCost(variables, alpha);
            velTrackCost = YalmipRotationOptimizationFactory.makeVelocityTrackingCost(variables, prediction, dXref, dYref, x, y, beta);
            zmpTrackCost = YalmipRotationOptimizationFactory.makeZMPTrackingCost(variables, prediction, x, y, gamma);
            stepDurCost = YalmipRotationOptimizationFactory.makeStepDurationCost(variables, delta);
            rotationCost = YalmipRotationOptimizationFactory.makeRotationCost(variables, xi, deltaTheta, thetac);
            
            cost = jerkCost + velTrackCost + zmpTrackCost + stepDurCost + rotationCost;
            
            integerConst = YalmipRotationOptimizationFactory.makeIntegerConstraints(variables);
            auxVarConst = YalmipRotationOptimizationFactory.makeAuxiliaryVariablesConstraints(variables);
            zmpErrorConst = YalmipRotationOptimizationFactory.makeZMPErrorConstraints(variables, prediction, dxsp, dysp, bsp, x, y);
            feetCollConst = YalmipRotationOptimizationFactory.makeFeetCollisionConstraints(variables, dxfr, dyfr, bfr, isLeftSwing, deltaTheta);
            jointsConst = YalmipRotationOptimizationFactory.makeJointsConstraints(variables, dxvl, dyvl, bvl, xa, ya, thetaa, T, deltaTheta, deltaThetaMax, thetac);
            currStepConst = YalmipRotationOptimizationFactory.makeCurrentStepConstraints(variables, xc, yc);
            maxAccelConst = YalmipRotationOptimizationFactory.makeAccelerationConstraints(variables, prediction, dxa, dya, ba, x, y);
            stepDurRefContConst = YalmipRotationOptimizationFactory.makeStepDurationReferenceContinuousConstraints(variables, nref, kprime);
            stepDurRefBinConst = YalmipRotationOptimizationFactory.makeStepDurationReferenceBinaryConstraints(variables, nref, kprime);
            footRotationConst = YalmipRotationOptimizationFactory.makeFootRotationConstraints(variables, Thetaref);
            boundConst = YalmipRotationOptimizationFactory.makeBoundConstraints(variables);
            
            constraints = integerConst + auxVarConst + zmpErrorConst + feetCollConst +...
                jointsConst + currStepConst + maxAccelConst + stepDurRefContConst +...
                stepDurRefBinConst + footRotationConst + boundConst;
            
            diagnostics = optimize(constraints, cost);
%             figure;
%             plot(times, 'LineWidth', 2);
%             title(sprintf('mean %g std %g', mean(times), std(times)));
%             xlabel('Iteration (-)', 'FontSize', 14);
%             ylabel('Solver Time (s)', 'FontSize', 14);
%             set(gca, 'FontSize', 14);

            d3X = value(variables.d3X);
            d3Y = value(variables.d3Y);
            Xfb = value(variables.Xfb);
            Yfb = value(variables.Yfb);
            Xfaux = value(variables.Xfaux);
            Yfaux = value(variables.Yfaux);
            value(variables.Ub)
            value(variables.Sr)
            value(variables.Bs)
            
            jerkCost = value(jerkCost)
            veltrackCost = value(velTrackCost)
            zmpTrackCost = value(zmpTrackCost)
            stepDurCost = value(stepDurCost)
            rotationCost = value(rotationCost)
            Thetafb = value(variables.Thetafb)
            Thetafaux = value(variables.Thetafaux)
            
            thetafb = deltaTheta * (0:Nr-1) * Thetafb';
            
            PlotterHumanoids.plotTest(d3X, d3Y, Xfb, Yfb, Xfaux, Yfaux,...
                thetafb, prediction, x, y, T, N, footDimensions);
        end
        
        function matrix = binaryVectorToMatrix(vector, N, Nf)
            matrix = zeros(N, Nf);
            
            for i=1:Nf
                for j=1:i
                    matrix(i, j) = vector(i*(i-1)/2+j);
                end
            end
            for i=Nf+1:N
                for j=1:Nf
                    matrix(i, j) = vector(Nf*(Nf+1)/2+(i-Nf-1)*Nf+j);
                end
            end
        end
        
        function testCases = makeIntegerConstraintsTestCases(N, Nf)
            Nbu = YalmipOptimizationFactory.getNbu(N, Nf);
            numTestCases = 2^Nbu;
            testCases = cell(1, numTestCases);
            
            for n=1:(numTestCases)
                array = YalmipTester.str2array(dec2bin(n - 1));
                array = [zeros(1, Nbu - length(array)), array];
                
                testCases{n} = array;
            end
        end

        function valid = verifyBinaryMatrix(matrix)
            sumLines = sum(matrix, 2);
            checkSumLines = (sumLines == ones(size(sumLines)));
            validSumLines = all(checkSumLines);
            lastNonZeroColumn = find(sum(matrix, 1) > 0, 1, 'last');
            firstZeroColumn = find(sum(matrix, 1) == 0, 1);
            validZeroColumns = (firstZeroColumn - lastNonZeroColumn == 1);
            if isempty(firstZeroColumn)
                validZeroColumns = true;
            elseif isempty(lastNonZeroColumn)
                validZeroColumns = false;
            end
            validNoBack = true;
            for j=2:size(matrix, 2)
                firstOne = find(matrix(:, j) == 1, 1);
                for i=firstOne:size(matrix, 1)
                    if matrix(i, j) - matrix(i, j - 1) < 0
                        validNoBack = false;
                        break;
                    end
                end
            end
            valid = validSumLines && validZeroColumns && validNoBack;
        end
        
        function array = str2array(string)
            array = zeros(1, length(string));
            for i=1:length(string)
                array(i) = str2num(string(i));
            end
        end
    end
end


