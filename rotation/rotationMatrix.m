function R = rotationMatrix(psi)

R = [cos(psi), -sin(psi); sin(psi), cos(psi)];

end