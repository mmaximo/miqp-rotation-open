function results = runSimulationNoPlotManyRotation()

amplitudes = 0:0.04:0.4;
directions = 0:(pi/6):pi;

numAmplitudes = length(amplitudes);
numDirections = length(directions);
numSimulations = numAmplitudes * numDirections;

results = zeros(numAmplitudes, numDirections);

T = 0.1;
N = 10;
Ncon = 18;

counter = 0;
for i=1:numAmplitudes
    for j=1:numDirections
        A = amplitudes(i);
        theta = directions(j);
        
        disturbances(1).kd = 2 * N * Ncon + 1;
        disturbances(1).time = disturbances(1).kd * T / Ncon;
        disturbances(1).xDist = [0; A * cos(theta); 0];
        disturbances(1).yDist = [0; A * sin(theta); 0];
        
        simulation = runSimulationNoPlotRotation(disturbances);
        
        if simulation.feasible
            results(i, j) = max(simulation.numFutureStepsh);
        else
            results(i, j) = 0;
        end
        
        counter = counter + 1;
        disp(sprintf('evaluating [%.2f, %.2f]; progress: %.2f',...
            amplitudes(i),...
            directions(j) * 180 / pi,...
            100 * (counter / numSimulations)));
    end
end

end