classdef Plotter < handle
    methods
        function self = Plotter()
        end
        
        function plotSimulation(self, X, Y, Zx, Zy, Xfoot, Yfoot, theta,...
                footDimensions, Ncon)
            [XfNumber, YfNumber, ThetafNumber] = toFootstepSpace(Xfoot, Yfoot, theta);
            
            h1 = plot(X(:, 1), Y(:, 1), 'b', 'LineWidth', 2);
            hold on;
            h2 = plot(Zx(:, 1), Zy(:, 1), 'r-', 'LineWidth', 2);
            plot(Zx(Ncon:Ncon:end, 1), Zy(Ncon:Ncon:end, 1), 'go', 'LineWidth', 2);
            xlabel('X ($m$)', 'FontSize', 14, 'interpreter', 'latex')
            ylabel('Y ($m$)', 'FontSize', 14, 'interpreter', 'latex')
            numSteps = size(Xfoot, 1);
            l = footDimensions.length;
            w = footDimensions.width;
            
            for s=1:length(XfNumber)%numSteps
                psi = ThetafNumber(s);%theta(s);
                center = [XfNumber(s); YfNumber(s)];
                R = rotationMatrix(psi);
                topLeft = center + R * [-l / 2; w / 2];
                topRight = center + R * [l / 2; w / 2];
                bottomLeft = center + R * [-l / 2; -w / 2];
                bottomRight = center + R * [l / 2; -w / 2];
                plot([topLeft(1), topRight(1), bottomRight(1), bottomLeft(1), topLeft(1), topRight(1)],...
                    [topLeft(2), topRight(2), bottomRight(2), bottomLeft(2), topLeft(2), topRight(2)],...
                    'k', 'LineWidth', 2);
%                 plotLine(topLeft, topRight, 'k');
%                 plotLine(topRight, bottomRight, 'k');
%                 plotLine(bottomRight, bottomLeft, 'k');
%                 plotLine(bottomLeft, topLeft, 'k');
            end
            
            for s=1:length(XfNumber)
                text(XfNumber(s), YfNumber(s), num2str(s), 'FontSize', 14, 'interpreter', 'latex');
            end
            
            axis equal
            grid on
            set(gca, 'FontSize', 14);
            set(gca, 'TickLabelInterpreter','latex')
            
            legend([h1, h2], {'CoM', 'ZMP'}, 'FontSize', 14, 'interpreter', 'latex')
        end
        
        function plot(self, X, Y, Zx, Zy, Xfoot, Yfoot, theta, footDimensions)
            plot(X(:, 1), Y(:, 1), 'b', 'LineWidth', 2);
            hold on;
            plot(Zx(:, 1), Zy(:, 1), 'r-+', 'LineWidth', 2);
            xlabel('X (m)', 'FontSize', 16)
            ylabel('Y (m)', 'FontSize', 16)
            legend('COM', 'ZMP')
            numSteps = size(Xfoot, 1);
            length = footDimensions.length;
            width = footDimensions.width;
            for s=1:numSteps
                psi = theta(s);
                center = [Xfoot(s); Yfoot(s)];
                R = rotationMatrix(psi);
                topLeft = center + R * [-length / 2; width / 2];
                topRight = center + R * [length / 2; width / 2];
                bottomLeft = center + R * [-length / 2; -width / 2];
                bottomRight = center + R * [length / 2; -width / 2];
                plotLine(topLeft, topRight, 'k');
                plotLine(topRight, bottomRight, 'k');
                plotLine(bottomRight, bottomLeft, 'k');
                plotLine(bottomLeft, topLeft, 'k');
            end
            axis equal
            grid on
            set(gca, 'FontSize', 16);
        end
    end
end

function [Xc, Yc, Thetac] = toFootstepSpace(Xch, Ych, Thetach)

tolerance = 10^-3;
index = 1;

Xc(1) = Xch(1);
Yc(1) = Ych(1);
Thetac(1) = Thetach(1);

for i=2:length(Xch)
    if abs(Ych(i) - Yc(index)) > tolerance
        index = index + 1;
    end
    Xc(index) = Xch(i);
    Yc(index) = Ych(i);
    Thetac(index) = Thetach(i);
end

end

function plotLine(point1, point2, color)

plot([point1(1); point2(1)], [point1(2); point2(2)], color, 'LineWidth', 2);

end