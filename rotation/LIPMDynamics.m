classdef LIPMDynamics < Dynamics
    properties
        T;
        zCOM;
    end
    
    methods
        function self = LIPMDynamics(T, zCOM)
            A = [1, T, T^2 / 2; 0, 1, T; 0, 0, 1];
            B = [T^3 / 6; T^2 / 2; T];
            C = [1, 0, -zCOM / Constants.GRAVITY];
            self = self@Dynamics(A, B, C);
            self.T = T;
            self.zCOM = zCOM;
        end
    end
end