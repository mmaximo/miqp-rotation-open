function makeSimulationVideo(times, simulation, footDimensions,...
    disturbances, config)

close all

set(0,'defaulttextinterpreter','latex')

figure('position', [0, 0, 800, 600]);

trail = config.Ncon * 2;

midX = (min(simulation.Xch) + max(simulation.Xch)) / 2;
midY = (min(simulation.Ych) + max(simulation.Ych)) / 2;

xmin = min(simulation.Xch) - footDimensions.length / 2;
xmax = max(simulation.Xch) + footDimensions.length / 2;
ymin = min(simulation.Ych) - footDimensions.length / 2;
ymax = max(simulation.Ych) + footDimensions.length / 2;
dx = xmax - xmin;
dy = ymax - ymin;

if dx / dy > 620.0 / 204.6977
    dy = dx * 204.6977 / 620.0;
    ymin = midY - dy / 2;
    ymax = midY + dy / 2;
else
    dx = dy * 620.0 / 204.6977;
    xmin = midX - dx / 2;
    xmax = midX + dx / 2;
end

c1 = [0, 0.4470, 0.7410];
c2 = [0.8500, 0.3250, 0.0980];

fontSize = 12;
distHold = 10;
i = 1;
k = 1;
gambi = false;
while i <= length(times)
% for i=1:length(times)
    subplot(2,2,3);
    hold off;
    yls = [-0.35, 0.35];
    plotWithDisturbance('Speed', '$m/s$', times(1:i), simulation.dXh(1:i), simulation.dYh(1:i),...
        disturbances, fontSize, yls);
    xlim([times(1) times(end)]);
    ylim(yls);
    subplot(2,2,4);
    hold off;
    yla = [-2, 2];
    plotWithDisturbance('Acceleration', '$m/s^2$', times(1:i), simulation.d2Xh(1:i), simulation.d2Yh(1:i),...
        disturbances, fontSize, yla);
    xlim([times(1) times(end)]);
    ylim(yla);
    hsub = subplot(2,2,[1 2]);
%     pos1 = getpixelposition(hsub)
    hold off;
    trailStart = i - trail;
    if trailStart < 1
        trailStart = 1;
    end
    ii = trailStart:i;
%     disturbances(1).kd
%     disturbances(1).xDist
%     disturbances(1).yDist
    if simulation.changingh(i)
        colorfa = [0.0, 0.0, 0.0];
    else
        colorfa = [0.8, 0.8, 0.8];
    end
    plotFoot(simulation.xah(i), simulation.yah(i), simulation.thetaah(i),...
        footDimensions, colorfa);
    hold on;
    plotFoot(simulation.Xch(i), simulation.Ych(i), simulation.Thetach(i),...
        footDimensions, [0, 0, 0]);
    plot(simulation.Zxh(ii), simulation.Zyh(ii), 'color', c2, 'LineWidth', 1);
    hzx = plot(simulation.Zxh(i), simulation.Zyh(i), '+', 'color', c2, 'LineWidth', 2);
    plot(simulation.Xh(ii), simulation.Yh(ii), 'color', c1, 'LineWidth', 1);
    hx = plot(simulation.Xh(i), simulation.Yh(i), '.', 'color', c1, 'MarkerSize', 20);
    if ~isempty(disturbances) && i == disturbances(1).kd - 1
%         disturbances(1).kd
%         disturbances(1).xDist
%         disturbances(1).yDist
        xDist = disturbances(1).xDist(2);
        yDist = disturbances(1).yDist(2);
        thetac = simulation.Thetach(i);
        rotatedDistX = cos(thetac) * xDist - sin(thetac) * yDist;
        rotatedDistY = sin(thetac) * xDist + cos(thetac) * yDist;
        pertDir = [rotatedDistX, rotatedDistY];
%         pertDir = [disturbances(1).xDist(2), disturbances(1).yDist(2)];
        pertDir = 0.05 * pertDir ./ norm(pertDir);
        x = [simulation.Xh(i) - pertDir(1), simulation.Xh(i)];
        y = [simulation.Yh(i) - pertDir(2), simulation.Yh(i)];
        drawArrow(x, y, 'g', 'LineWidth', 2);
        distHold = distHold - 1;
        if distHold > 0
            i = i - 1;
        end
    end
    xlabel('X ($m$)', 'FontSize', fontSize);
    ylabel('Y ($m$)', 'FontSize', fontSize);
    legend([hx, hzx], 'CoM', 'ZMP', 'interpreter', 'latex', 'FontSize', 12);
    title(config.title, 'FontSize', fontSize);
    set(gca, 'FontSize', fontSize, 'LineWidth', 1);
    set(gca, 'TickLabelInterpreter','latex')
    xlim([xmin, xmax]);
    ylim([ymin, ymax]);
    
    if ~gambi
        gambi = true;
        continue;
    end
%     grid on;
    
%     saveas(gcf, sprintf('video/figure%d.png', i));
    print('-depsc2', sprintf('video/figure%04d.eps', k));
    i = i + 1;
    k = k + 1;
end

end

function h = plotFoot(xfc, yfc, thetac, footDimensions, rgb)
    len = footDimensions.length;
    wid = footDimensions.width;
    psi = thetac;
    center = [xfc; yfc];
    R = rotationMatrix(psi);
    topLeft = center + R * [-len / 2; wid / 2];
    topRight = center + R * [len / 2; wid / 2];
    bottomLeft = center + R * [-len / 2; -wid / 2];
    bottomRight = center + R * [len / 2; -wid / 2];
    pointsX = [topLeft(1), topRight(1), bottomRight(1), bottomLeft(1),...
        topLeft(1), topRight(1)];
    pointsY = [topLeft(2), topRight(2), bottomRight(2), bottomLeft(2),...
        topLeft(2), topRight(2)];
    h = plot(pointsX, pointsY, 'Color', rgb, 'LineWidth', 2);
%     h(1) = plotLine(topLeft, topRight, 'k');
%     h(2) = plotLine(topRight, bottomRight, 'k');
%     h(3) = plotLine(bottomRight, bottomLeft, 'k');
%     h(4) = plotLine(bottomLeft, topLeft, 'k');
end

function plotWithDisturbance(plotType, unit, times, valuesX, valuesY,...
    disturbances, fontSize, yl)

c1 = [0, 0.4470, 0.7410];
c2 = [0.8500, 0.3250, 0.0980];

% figure;
plot(times, valuesX, 'LineWidth', 1, 'color', c1);
hold on;
plot(times, valuesY, 'r', 'LineWidth', 1, 'color', c2);
for p=1:length(disturbances)
    if disturbances(p).kd-1 > length(times)
        continue;
    end
    px = [disturbances(p).time, disturbances(p).time];
%     py = [min(min(valuesX), min(valuesY)),...
%            max(max(valuesX), max(valuesY))];
    plot(px, yl, 'k-.', 'LineWidth', 1);
end
% ah1 = gca;
% ah2 = axes('position',get(gca,'position'), 'visible','off');
% legend(ah1, h1, 'Location', [0.5 0.85 0.15 0.05], sprintf('x %s', lower(plotType)))
% legend(ah2, h2, 'Location', [0.7 0.85 0.15 0.05], sprintf('y %s', lower(plotType)))
legend({'x', 'y'}, 'interpreter', 'latex', 'FontSize', 12);
% legend(sprintf('x %s', lower(plotType)), sprintf('y %s', lower(plotType)));
xlabel('Time ($s$)', 'FontSize', fontSize);
ylabel(sprintf('%s (%s)', plotType, unit), 'FontSize', fontSize);
set(gca, 'FontSize', fontSize,  'LineWidth', 1);
set(gca, 'TickLabelInterpreter','latex')
% grid on;

end

function plotLine(point1, point2, color)

plot([point1(1); point2(1)], [point1(2); point2(2)], color, 'LineWidth', 2);

end

function drawArrow(x, y, varargin)

quiver( x(1),y(1),x(2)-x(1),y(2)-y(1),0, varargin{:} );

end