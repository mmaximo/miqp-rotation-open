classdef PlotterHumanoids < handle
    methods(Static)
        function plotTest(d3X, d3Y, Xfb, Yfb, Xfaux, Yfaux, theta,...
                prediction, x, y, T, N, footDimensions)
            lineWidth = 2;
            fontSize = 16;
            
            times = 0:T:(N-1)*T;
            X = prediction.Pps * x + prediction.Ppu * d3X;
            Y = prediction.Pps * y + prediction.Ppu * d3Y;
            dX = prediction.Pvs * x + prediction.Pvu * d3X;
            dY = prediction.Pvs * y + prediction.Pvu * d3Y;
            d2X = prediction.Pas * x + prediction.Pau * d3X;
            d2Y = prediction.Pas * y + prediction.Pau * d3Y;
            Zx = prediction.Pzs * x + prediction.Pzu * d3X;
            Zy = prediction.Pzs * y + prediction.Pzu * d3Y;
            figure;
            hold on;
            plot(times, d3X, 'LineWidth', lineWidth);
            plot(times, d3Y, 'r', 'LineWidth', lineWidth);
            xlabel('Time [s]', 'FontSize', fontSize);
            ylabel('Jerk [m/s^3]', 'FontSize', fontSize);
            legend('X Jerk', 'Y Jerk');
            grid on;
            set(gca, 'FontSize', fontSize);
            figure;
            hold on;
            plot(times, dX, 'LineWidth', lineWidth);
            plot(times, dY, 'r', 'LineWidth', lineWidth);
            xlabel('Time [s]', 'FontSize', fontSize);
            ylabel('Speed [m/s]', 'FontSize', fontSize);
            legend('X Speed', 'Y Speed');
            grid on;
            set(gca, 'FontSize', fontSize);
            figure;
            hold on;
            plot(times, d2X, 'LineWidth', lineWidth);
            plot(times, d2Y, 'r', 'LineWidth', lineWidth);
            xlabel('Time [s]', 'FontSize', fontSize);
            ylabel('Acceleration [m/s^2]', 'FontSize', fontSize);
            legend('X Acceleration', 'Y Acceleration');
            grid on;
            set(gca, 'FontSize', fontSize);
            figure;
            hold on;
            plot(times, Xfaux, 'LineWidth', lineWidth);
            plot(times, Yfaux, 'r', 'LineWidth', lineWidth);
            plot(times, Zx, '--', 'LineWidth', lineWidth);
            plot(times, Zy, 'r--', 'LineWidth', lineWidth);
            xlabel('Time [s]', 'FontSize', fontSize);
            ylabel('Foot Position [m]', 'FontSize', fontSize);
            legend('X Foot Position', 'Y Foot Position');
            grid on;
            set(gca, 'FontSize', fontSize);
            
            figure;
            hold on;
%             numSteps = size(Xfaux, 1);
            numSteps = size(Xfb, 1);
            length = footDimensions.length;
            width = footDimensions.width;
            for s=1:numSteps
                psi = theta(s);
                center = [Xfb(s); Yfb(s)];
%                 center = [Xfaux(s); Yfaux(s)];
                R = rotationMatrix(psi);
                topLeft = center + R * [-length / 2; width / 2];
                topRight = center + R * [length / 2; width / 2];
                bottomLeft = center + R * [-length / 2; -width / 2];
                bottomRight = center + R * [length / 2; -width / 2];
                footContourX = [topLeft(1), topRight(1), bottomRight(1),...
                    bottomLeft(1), topLeft(1)];
                footContourY = [topLeft(2), topRight(2), bottomRight(2),...
                    bottomLeft(2), topLeft(2)];
                plot(footContourX, footContourY, 'k', 'LineWidth', lineWidth);
            end
            h1 = plot(X, Y, 'b', 'LineWidth', lineWidth);
            h2 = plot(Zx, Zy, 'r-', 'LineWidth', lineWidth);
            xlabel('X (m)', 'FontSize', fontSize)
            ylabel('Y (m)', 'FontSize', fontSize)
            legend([h1 h2], 'CoM', 'ZMP')
            axis equal
            grid on
            set(gca, 'FontSize', fontSize);
        end
    end
end