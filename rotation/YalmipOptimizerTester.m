classdef YalmipOptimizerTester < handle
    methods(Static)
        function testStepDurationReferenceConstraints()
            N = 10;
            Nf = 3;
            delta = 100;
            nref = 5;
            kprime = 3;
            
            Nbu = Nf * (Nf + 1) / 2 + (N - Nf) * Nf;
            variables.N = N;
            variables.Nf = Nf;
            variables.Nbu = Nbu;
            variables.Sr = sdpvar(Nf, 1);
            variables.Bu = binvar(Nbu, 1);
            variables.Ub = YalmipOptimizerFactory.makeUb(variables.Bu, N, Nf);
            variables.Bs = binvar(Nf, 1);
            
            variablesVector = {
                variables.Sr,...
                variables.Bu,...
                variables.Ub,...
                variables.Bs
                };
            
            params.kprime = sdpvar;
            
            paramsVector = {
                params.kprime
                };
            
            stepDurCost = YalmipOptimizerFactory.makeStepDurationCost(variables, delta);
            
            integerConst = YalmipOptimizerFactory.makeIntegerConstraints(variables);
            stepDurRefContConst = YalmipOptimizerFactory.makeStepDurationReferenceContinuousConstraints(variables, params, nref);
            stepDurRefBinConst = YalmipOptimizerFactory.makeStepDurationReferenceBinaryConstraints(variables, params, nref);
            
            boundsConst = [zeros(Nf, 1) <= variables.Sr <= N * ones(Nf, 1)];
            
            constraints = integerConst + stepDurRefContConst + stepDurRefBinConst + boundsConst;
            
            options = sdpsettings;
            
            problem = optimizer(constraints, stepDurCost, options, paramsVector, variablesVector);
            
            paramsValues = {
                kprime
                };
                
            
            solution = problem(paramsValues);
            
            solution{1}
            solution{2}
            solution{3}
            solution{4}
            
            value(variables.Sr)
            YalmipOptimizerTester.binaryVectorToMatrix(value(variables.Bu), N, Nf)
            variables.Ub
            value(variables.Ub)
            value(variables.Bs)
        end
        
        function testJerkCost()
            alpha = 10^-6;
            beta = 1;
            N = 10;
            Nf = 3;
            Nr = 10;
            nref = 5;
            kprime = 0;
            delta = 100;
            T = 0.1;
            zCOM = 0.2;
            footDimensions = struct;
            footDimensions.length = 0.6;
            footDimensions.width = 0.3;
            
            prediction = Prediction(LIPMDynamics(T, zCOM), N);
            
            x = [0; 0; 0];
            y = [0; 0; 0];
            dXr = ones(N, 1);
            dYr = ones(N, 1);
            
            variables = YalmipOptimizerFactory.makeVariables(N, Nf, Nr);
            
            variablesCell = YalmipOptimizerFactory.makeVariablesCell(variables);
            
            params.dXr = sdpvar(N, 1);
            params.dYr = sdpvar(N, 1);
            params.kprime = sdpvar;
            params.x = sdpvar(3, 1);
            params.y = sdpvar(3, 1);
            
            paramsCell = {
                params.dXr,...
                params.dYr,...
                params.kprime,...
                params.x,...
                params.y
                };
            
            paramsValues = {
                dXr,...
                dYr,...
                kprime,...
                x,...
                y,...
                };
            
            jerkCost = YalmipOptimizerFactory.makeJerkCost(variables, alpha);
            velTrackCost = YalmipOptimizerFactory.makeVelocityTrackingCost(variables, params, prediction, beta);
            stepDurCost = YalmipOptimizerFactory.makeStepDurationCost(variables, delta);
            
            cost = jerkCost + velTrackCost + stepDurCost;
            
            integerConst = YalmipOptimizerFactory.makeIntegerConstraints(variables);
            stepDurRefContConst = YalmipOptimizerFactory.makeStepDurationReferenceContinuousConstraints(variables, params, nref);
            stepDurRefBinConst = YalmipOptimizerFactory.makeStepDurationReferenceBinaryConstraints(variables, params, nref);
            boundConst = YalmipOptimizerFactory.makeBoundConstraintsOnlyVariables(variables);
            
            constraints = integerConst + stepDurRefContConst + stepDurRefBinConst + boundConst;
            
            options = sdpsettings('solver', 'gurobi');
            
            problem = optimizer(constraints, cost, options, paramsCell, variablesCell);
            
            solution = problem(paramsValues);
            
            d3X = solution{1};
            Xfb = solution{2};
            d3Y = solution{3};
            Yfb = solution{4};
            Xfaux = solution{5};
            Yfaux = solution{6};
            
            theta = zeros(N, 1);
            
            PlotterHumanoids.plotTest(d3X, d3Y, Xfb, Yfb, Xfaux, Yfaux,...
                theta, prediction, x, y, T, N, footDimensions);
        end
        
        function testZMPSupportPolygon()
            alpha = 10^-6;
            beta = 1;
            gamma = 10^-6;
            N = 10;
            Nf = 3;
            Nr = 11;
            nref = 5;
            kprime = 0;
            delta = 100;
            xi = 100;
            T = 0.1;
            zCOM = 0.2;
            footDimensions = struct;
            footDimensions.length = 0.06;
            footDimensions.width = 0.03;
            xc = 0;
            yc = 0;
            thetac = 0;
            bls = 0;
            bccw = 1;
            
            Thetafp = zeros(Nr, 1);
            
            prediction = Prediction(LIPMDynamics(T, zCOM), N);
            
            x = [0; 0; 0];
            y = [0; 0; 0];
            dXr = 0.1 * ones(N, 1);
            dYr = 0.1 * ones(N, 1);
            dthetar = 0;
            
            variables = YalmipOptimizerFactory.makeVariables(N, Nf, Nr);
            
            variablesCell = YalmipOptimizerFactory.makeVariablesCell(variables);
            
            params.Nr = Nr;
            params.dXr = sdpvar(N, 1);
            params.dYr = sdpvar(N, 1);
            params.dthetar = sdpvar;
            params.kprime = sdpvar;
            params.x = sdpvar(3, 1);
            params.y = sdpvar(3, 1);
            params.xc = sdpvar;
            params.yc = sdpvar;
            params.thetac = sdpvar;
            params.bls = sdpvar;
            params.bccw = sdpvar;
            params.Thetafp = sdpvar(Nr, 1);
            params.cfp = sdpvar(Nr, 1);
            params.sfp = sdpvar(Nr, 1);
            
            paramsCell = {
                params.dXr,...
                params.dYr,...
                params.dthetar,...
                params.kprime,...
                params.x,...
                params.y,...
                params.xc,...
                params.yc,...
                params.thetac,...
                params.bls,...
                params.bccw,...
                params.Thetafp,...
                params.cfp,...
                params.sfp
                };
            
            paramsValues = {
                dXr,...
                dYr,...
                dthetar,...
                kprime,...
                x,...
                y,...
                xc,...
                yc,...
                thetac,...
                bls,...
                bccw,...
                Thetafp,...
                cos(Thetafp),...
                sin(Thetafp)
                };
            
            [dxsp, dysp, bsp] = YalmipOptimizerFactory.makeSupportPolygon(params, footDimensions.length, footDimensions.width);
            
            jerkCost = YalmipOptimizerFactory.makeJerkCost(variables, alpha);
            velTrackCost = YalmipOptimizerFactory.makeVelocityTrackingCost(variables, params, prediction, beta);
            zmpTrackCost = YalmipOptimizerFactory.makeZMPTrackingCost(variables, params, prediction, gamma);
            stepDurCost = YalmipOptimizerFactory.makeStepDurationCost(variables, delta);
            rotationCost = YalmipOptimizerFactory.makeRotationCost(variables, params, xi);
            
            cost = jerkCost + velTrackCost + zmpTrackCost + stepDurCost + rotationCost;
            
            integerConst = YalmipOptimizerFactory.makeIntegerConstraints(variables);
            auxVarConst = YalmipOptimizerFactory.makeAuxiliaryVariablesConstraints(variables);
            zmpErrorConst = YalmipOptimizerFactory.makeZMPErrorConstraints(variables, params, prediction, dxsp, dysp, bsp);
            currStepConst = YalmipOptimizerFactory.makeCurrentStepConstraints(variables, params);
            stepDurRefContConst = YalmipOptimizerFactory.makeStepDurationReferenceContinuousConstraints(variables, params, nref);
            stepDurRefBinConst = YalmipOptimizerFactory.makeStepDurationReferenceBinaryConstraints(variables, params, nref);
            footRotationConst = YalmipOptimizerFactory.makeFootRotationConstraints(variables, params, nref, T);
            boundConst = YalmipOptimizerFactory.makeBoundConstraintsOnlyVariables(params);
            
            constraints = integerConst + auxVarConst + zmpErrorConst +...
                currStepConst + stepDurRefContConst + stepDurRefBinConst +...
                footRotationConst + boundConst;
            
            options = sdpsettings('solver', 'gurobi');
            
            problem = optimizer(constraints, cost, options, paramsCell, variablesCell);
            
            solution = problem(paramsValues);
            
            d3X = value(variables.d3X);
            d3Y = value(variables.d3Y);
            Xfb = value(variables.Xfb);
            Yfb = value(variables.Yfb);
            Xfaux = value(variables.Xfaux);
            Yfaux = value(variables.Yfaux);
            Thetafb = value(variables.Thetafb);
            Thetafaux = value(variables.Thetafaux);
            
            Xfb
            Xfaux
            Ub = value(variables.Ub)
            Thetafb
            Thetafaux
            variables.Thetafb
            variables.Thetafaux
            
            PlotterHumanoids.plotTest(d3X, d3Y, Xfb, Yfb, Xfaux, Yfaux,...
                Thetafp, prediction, x, y, T, N, footDimensions);
        end
        
        function testZMPFeetCollision()
            alpha = 10^-6;
            beta = 1;
            gamma = 10^-6;
            N = 10;
            Nf = 3;
            Nr = 11;
            nref = 5;
            kprime = 0;
            delta = 100;
            xi = 100;
            T = 0.1;
            zCOM = 0.2;
            footDimensions = struct;
            footDimensions.length = 0.06;
            footDimensions.width = 0.03;
            xc = 0;
            yc = 0;
            thetac = 0;
            ySep = 0.1;
            bls = 1;
            bccw = 1;
            
            theta = zeros(Nr, 1);
            
            prediction = Prediction(LIPMDynamics(T, zCOM), N);
            
            x = [0; 0; 0];
            y = [0; 0; 0];
            dXr = 0.1 * ones(N, 1);
            dYr = zeros(N, 1);
            dthetar = 0;
            Thetafp = zeros(Nr, 1);
            cfp = cos(Thetafp);
            sfp = sin(Thetafp);
            
            variables = YalmipOptimizerFactory.makeVariables(N, Nf, Nr);
            
            variablesCell = YalmipOptimizerFactory.makeVariablesCell(variables);
            
            params.Nr = Nr;
            params.dXr = sdpvar(N, 1);
            params.dYr = sdpvar(N, 1);
            params.dthetar = sdpvar;
            params.kprime = sdpvar;
            params.x = sdpvar(3, 1);
            params.y = sdpvar(3, 1);
            params.xc = sdpvar;
            params.yc = sdpvar;
            params.thetac = sdpvar;
            params.bls = sdpvar;
            params.bccw = sdpvar;
            params.Thetafp = sdpvar(Nr, 1);
            params.cfp = sdpvar(Nr, 1);
            params.sfp = sdpvar(Nr, 1);
            
            paramsCell = {
                params.dXr,...
                params.dYr,...
                params.dthetar,...
                params.kprime,...
                params.x,...
                params.y,...
                params.xc,...
                params.yc,...
                params.thetac,...
                params.bls,...
                params.bccw,...
                params.Thetafp,...
                params.cfp,...
                params.sfp
                };
            
            paramsValues = {
                dXr,...
                dYr,...
                dthetar,...
                kprime,...
                x,...
                y,...
                xc,...
                yc,...
                thetac,...
                bls,...
                bccw,...
                Thetafp,...
                cfp,...
                sfp
                };
            
            [dxfr, dyfr, bfr] = YalmipOptimizerFactory.makeFootReachability(params, ySep);
            [dxsp, dysp, bsp] = YalmipOptimizerFactory.makeSupportPolygon(params, footDimensions.length, footDimensions.width);
            
            jerkCost = YalmipOptimizerFactory.makeJerkCost(variables, alpha);
            velTrackCost = YalmipOptimizerFactory.makeVelocityTrackingCost(variables, params, prediction, beta);
            zmpTrackCost = YalmipOptimizerFactory.makeZMPTrackingCost(variables, params, prediction, gamma);
            stepDurCost = YalmipOptimizerFactory.makeStepDurationCost(variables, delta);
            rotationCost = YalmipOptimizerFactory.makeRotationCost(variables, params, xi);
            
            cost = jerkCost + velTrackCost + zmpTrackCost + stepDurCost + rotationCost;
            
            integerConst = YalmipOptimizerFactory.makeIntegerConstraints(variables);
            auxVarConst = YalmipOptimizerFactory.makeAuxiliaryVariablesConstraints(variables);
            zmpErrorConst = YalmipOptimizerFactory.makeZMPErrorConstraints(variables, params, prediction, dxsp, dysp, bsp);
            feetCollConst = YalmipOptimizerFactory.makeFeetCollisionConstraints(variables, params, dxfr, dyfr, bfr);
            currStepConst = YalmipOptimizerFactory.makeCurrentStepConstraints(variables, params);
            stepDurRefContConst = YalmipOptimizerFactory.makeStepDurationReferenceContinuousConstraints(variables, params, nref);
            stepDurRefBinConst = YalmipOptimizerFactory.makeStepDurationReferenceBinaryConstraints(variables, params, nref);
            footRotationConst = YalmipOptimizerFactory.makeFootRotationConstraints(variables, params, nref, T);
            boundConst = YalmipOptimizerFactory.makeBoundConstraintsOnlyVariables(variables);
            
            constraints = integerConst + auxVarConst + zmpErrorConst +...
                feetCollConst + currStepConst + stepDurRefContConst + stepDurRefBinConst +...
                footRotationConst + boundConst;
            
            options = sdpsettings('solver', 'gurobi');
            
            problem = optimizer(constraints, cost, options, paramsCell, variablesCell);
            
            problem(paramsValues);
            
            d3X = value(variables.d3X);
            d3Y = value(variables.d3Y);
            Xfb = value(variables.Xfb);
            Yfb = value(variables.Yfb);
            Xfaux = value(variables.Xfaux);
            Yfaux = value(variables.Yfaux);
            
            PlotterHumanoids.plotTest(d3X, d3Y, Xfb, Yfb, Xfaux, Yfaux,...
                theta, prediction, x, y, T, N, footDimensions);
        end
        
        function testFullMIQP()
            alpha = 10^-6;
            beta = 1;
            gamma = 10^-6;
            N = 10;
            Nf = 3;
            Nr = 11;
            nref = 5;
            kprime = 0;
            delta = 1000;
            xi = 1000;
            T = 0.1;
            zCOM = 0.2;
            footDimensions = struct;
            footDimensions.length = 0.06;
            footDimensions.width = 0.03;
            xc = 0;
            yc = 0;
            thetac = 0;
            bls = 0;
            ySep = 0.1;
            xa = 0;
            ya = 0;
            thetaa = 0;
            axmax = 10;
            aymax = 10;
            vxmax = 10;
            vymax = 10;
            dthetaMax = 1;
            
            prediction = Prediction(LIPMDynamics(T, zCOM), N);
            
            x = [0; 0; 0];
            y = [0; 0; 0];
            dXr = 0.1 * ones(N, 1);
            dYr = zeros(N, 1);
            dthetar = -0.1;
            if dthetar >= 0
                bccw = 1;
            else
                bccw = 0;
            end
            Thetafp = dthetar * T * (0:Nr-1)';
            cfp = cos(Thetafp);
            sfp = sin(Thetafp);
            
            variables = YalmipOptimizerFactory.makeVariables(N, Nf, Nr);
            
            variablesCell = YalmipOptimizerFactory.makeVariablesCell(variables);
            
            params = YalmipOptimizerFactory.makeParams(N, Nr);
            
            paramsCell = YalmipOptimizerFactory.makeParamsCell(params);
            
            paramsValues = {
                dXr,...
                dYr,...
                dthetar,...
                kprime,...
                x,...
                y,...
                xc,...
                yc,...
                thetac,...
                xa,...
                ya,...
                thetaa,...
                bls,...
                bccw,...
                Thetafp,...
                cfp,...
                sfp
                };
            
            [dxfr, dyfr, bfr] = YalmipOptimizerFactory.makeFootReachability(params, ySep);
            [dxsp, dysp, bsp] = YalmipOptimizerFactory.makeSupportPolygon(params, footDimensions.length, footDimensions.width);
            [dxvl, dyvl, bvl] = YalmipOptimizerFactory.makeVelocityLimits(params, vxmax, vymax);
            [dxa, dya, ba] = YalmipOptimizerFactory.makeAccelerationLimits(params, axmax, aymax);
             
            jerkCost = YalmipOptimizerFactory.makeJerkCost(variables, alpha);
            velTrackCost = YalmipOptimizerFactory.makeVelocityTrackingCost(variables, params, prediction, beta);
            zmpTrackCost = YalmipOptimizerFactory.makeZMPTrackingCost(variables, params, prediction, gamma);
            stepDurCost = YalmipOptimizerFactory.makeStepDurationCost(variables, delta);
            rotationCost = YalmipOptimizerFactory.makeRotationCost(variables, params, xi);
            
            cost = jerkCost + velTrackCost + zmpTrackCost + stepDurCost + rotationCost;
            
            integerConst = YalmipOptimizerFactory.makeIntegerConstraints(variables);
            auxVarConst = YalmipOptimizerFactory.makeAuxiliaryVariablesConstraints(variables);
            zmpErrorConst = YalmipOptimizerFactory.makeZMPErrorConstraints(variables, params, prediction, dxsp, dysp, bsp);
            feetCollConst = YalmipOptimizerFactory.makeFeetCollisionConstraints(variables, params, dxfr, dyfr, bfr);
            jointsConst = YalmipOptimizerFactory.makeJointsConstraints(variables, params, dxvl, dyvl, bvl, T, dthetaMax);
            currStepConst = YalmipOptimizerFactory.makeCurrentStepConstraints(variables, params);
            maxAccelConst = YalmipOptimizerFactory.makeAccelerationConstraints(variables, params, prediction, dxa, dya, ba);
            stepDurRefContConst = YalmipOptimizerFactory.makeStepDurationReferenceContinuousConstraints(variables, params, nref);
            stepDurRefBinConst = YalmipOptimizerFactory.makeStepDurationReferenceBinaryConstraints(variables, params, nref);
            footRotationConst = YalmipOptimizerFactory.makeFootRotationConstraints(variables, params, nref, T);
            boundConst = YalmipOptimizerFactory.makeBoundConstraints(variables, params);
            
            constraints = integerConst + auxVarConst + zmpErrorConst + feetCollConst +...
                jointsConst + currStepConst + maxAccelConst + stepDurRefContConst +...
                stepDurRefBinConst + footRotationConst + boundConst;
            
            options = sdpsettings('solver', 'gurobi');
            
            problem = optimizer(constraints, cost, options, paramsCell, variablesCell);
            
            solution = problem(paramsValues);

            d3X = value(variables.d3X);
            d3Y = value(variables.d3Y);
            Xfb = value(variables.Xfb);
            Yfb = value(variables.Yfb);
            Xfaux = value(variables.Xfaux);
            Yfaux = value(variables.Yfaux);
            value(variables.Ub)
            value(variables.Sr)
            value(variables.Bs)
            
            jerkCost = value(jerkCost)
            veltrackCost = value(velTrackCost)
            zmpTrackCost = value(zmpTrackCost)
            stepDurCost = value(stepDurCost)
            rotationCost = value(rotationCost)
            Thetafb = value(variables.Thetafb)
            Thetafaux = value(variables.Thetafaux)
            
            thetafb = Thetafp' * Thetafb';
            
            PlotterHumanoids.plotTest(d3X, d3Y, Xfb, Yfb, Xfaux, Yfaux,...
                thetafb, prediction, x, y, T, N, footDimensions);
        end
        
        function matrix = binaryVectorToMatrix(vector, N, Nf)
            matrix = zeros(N, Nf);
            
            for i=1:Nf
                for j=1:i
                    matrix(i, j) = vector(i*(i-1)/2+j);
                end
            end
            for i=Nf+1:N
                for j=1:Nf
                    matrix(i, j) = vector(Nf*(Nf+1)/2+(i-Nf-1)*Nf+j);
                end
            end
        end
        
        function testCases = makeIntegerConstraintsTestCases(N, Nf)
            Nbu = YalmipOptimizationFactory.getNbu(N, Nf);
            numTestCases = 2^Nbu;
            testCases = cell(1, numTestCases);
            
            for n=1:(numTestCases)
                array = YalmipTester.str2array(dec2bin(n - 1));
                array = [zeros(1, Nbu - length(array)), array];
                
                testCases{n} = array;
            end
        end

        function valid = verifyBinaryMatrix(matrix)
            sumLines = sum(matrix, 2);
            checkSumLines = (sumLines == ones(size(sumLines)));
            validSumLines = all(checkSumLines);
            lastNonZeroColumn = find(sum(matrix, 1) > 0, 1, 'last');
            firstZeroColumn = find(sum(matrix, 1) == 0, 1);
            validZeroColumns = (firstZeroColumn - lastNonZeroColumn == 1);
            if isempty(firstZeroColumn)
                validZeroColumns = true;
            elseif isempty(lastNonZeroColumn)
                validZeroColumns = false;
            end
            validNoBack = true;
            for j=2:size(matrix, 2)
                firstOne = find(matrix(:, j) == 1, 1);
                for i=firstOne:size(matrix, 1)
                    if matrix(i, j) - matrix(i, j - 1) < 0
                        validNoBack = false;
                        break;
                    end
                end
            end
            valid = validSumLines && validZeroColumns && validNoBack;
        end
        
        function array = str2array(string)
            array = zeros(1, length(string));
            for i=1:length(string)
                array(i) = str2num(string(i));
            end
        end
    end
end


