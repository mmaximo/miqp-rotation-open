classdef YalmipOptimizerFactory < handle
    methods(Static)
        function variables = makeVariables(N, Nf, Nr)
            Nbu = YalmipRotationOptimizationFactory.getNbu(N, Nf);
            variables.N = N;
            variables.Nf = Nf;
            variables.Nr = Nr;
            variables.Nbu = Nbu;
            variables.d3X = sdpvar(N, 1);
            variables.Xfb = sdpvar(Nf, 1);
            variables.d3Y = sdpvar(N, 1);
            variables.Yfb = sdpvar(Nf, 1);
            variables.Xfaux = sdpvar(N, 1);
            variables.Yfaux = sdpvar(N, 1);
            variables.Sr = sdpvar(Nf, 1);
            variables.Bu = binvar(Nbu, 1);
            variables.Ub = YalmipOptimizerFactory.makeUb(variables.Bu, N, Nf);
            variables.Bs = binvar(Nf, 1);
            variables.Thetafb = binvar(Nf, Nr, 'full');
            variables.Thetafaux = binvar(N, Nr, 'full');
            variables.Thetar = sdpvar(Nf, 1);
            variables.deltaThetar = sdpvar(Nf, 1);
        end
        
        function variablesCell = makeVariablesCell(variables)
            variablesCell = {
                variables.d3X,...       % 1
                variables.Xfb,...       % 2
                variables.d3Y,...       % 3
                variables.Yfb,...       % 4
                variables.Xfaux,...     % 5
                variables.Yfaux,...     % 6
                variables.Sr,...        % 7
                variables.Bu,...        % 8
                variables.Ub,...        % 9
                variables.Bs,...        % 10
                variables.Thetafb,...   % 11
                variables.Thetafaux,... % 12
                variables.Thetar,...    % 13
                variables.deltaThetar   % 14
                };
        end
        
        function params = makeParams(N, Nr)
            params.N = N;
            params.Nr = Nr;
            params.dXr = sdpvar(N, 1);
            params.dYr = sdpvar(N, 1);
            params.dthetar = sdpvar;
            params.kprime = sdpvar;
            params.x = sdpvar(3, 1);
            params.y = sdpvar(3, 1);
            params.xc = sdpvar;
            params.yc = sdpvar;
            params.thetac = sdpvar;
            params.xa = sdpvar;
            params.ya = sdpvar;
            params.thetaa = sdpvar;
            params.bds = sdpvar;
            params.bls = sdpvar;
            params.bccw = sdpvar;
            params.Thetafp = sdpvar(Nr, 1);
            params.cfp = sdpvar(Nr, 1);
            params.sfp = sdpvar(Nr, 1);
        end
        
        function paramsCell = makeParamsCell(params)
            paramsCell = {
                params.dXr,...
                params.dYr,...
                params.dthetar,...
                params.kprime,...
                params.x,...
                params.y,...
                params.xc,...
                params.yc,...
                params.thetac,...
                params.xa,...
                params.ya,...
                params.thetaa,...
                params.bds,...
                params.bls,...
                params.bccw,...
                params.Thetafp,...
                params.cfp,...
                params.sfp
                };
        end
        
        function Nbu = getNbu(N, Nf)
            Nbu = Nf * (Nf + 1) / 2 + (N - Nf) * Nf;
        end
        
        function Ub = makeUb(Bu, N, Nf)
            for j=1:Nf
                for i=j:Nf
                    Ub(i, j) = Bu(i*(i-1)/2+j);
                end
                for i=Nf+1:N
                    Ub(i, j) = Bu(Nf*(Nf+1)/2+(i-Nf-1)*Nf+j);
                end
            end
        end
        
        function cost = makeJerkCost(variables, alpha)
            cost = (alpha / 2) * variables.d3X' * variables.d3X +...
                (alpha / 2) * variables.d3Y' * variables.d3Y;
        end
        
        function cost = makeVelocityTrackingCost(variables, params, prediction, beta)
            velocityErrorX = params.dXr - (prediction.Pvs * params.x + prediction.Pvu * variables.d3X);
            velocityErrorY = params.dYr - (prediction.Pvs * params.y + prediction.Pvu * variables.d3Y);
            cost = (beta / 2) * (velocityErrorX' * velocityErrorX) +...
                (beta / 2) * (velocityErrorY' * velocityErrorY);
        end
        
        function cost = makeZMPTrackingCost(variables, params, prediction, gamma)
            zmpErrorX = variables.Xfaux - (prediction.Pzs * params.x + prediction.Pzu * variables.d3X);
            zmpErrorY = variables.Yfaux - (prediction.Pzs * params.y + prediction.Pzu * variables.d3Y);
            cost = (gamma / 2) * (zmpErrorX' * zmpErrorX) +...
                (gamma / 2) * (zmpErrorY' * zmpErrorY);
        end
        
        function cost = makeStepDurationCost(variables, delta)
            N = variables.N;
            Nf = variables.Nf;
            for j=1:Nf
                S(j, 1) = sum(variables.Ub(j:N, j));
            end
            cost = (delta / 2) * ((variables.Sr - S)' * (variables.Sr - S));
        end
        
        function cost = makeRotationCost(variables, params, xi)
            Nf = variables.Nf;
            for j=2:Nf
                Theta(j, 1) = params.Thetafp' * variables.Thetafb(j, :)';
            end
            cost = (xi / 2) * ((variables.Thetar - Theta)' * (variables.Thetar - Theta));
        end
        
        function [dxfr, dyfr, bfr] = makeFootReachability(params, ySep)
            dxfr = cell(1, params.Nr);
            dyfr = cell(1, params.Nr);
            
            for i=1:params.Nr
                dxfr{i} = -params.sfp(i);
                dyfr{i} = params.cfp(i);
            end
            
            bfr = -ySep;
        end
        
        function [dxvl, dyvl, bvl] = makeVelocityLimits(params, vxmax, vymax)
            dxvl = cell(1, params.Nr);
            dyvl = cell(1, params.Nr);
            
            for i=1:params.Nr
                dxvl{i} = [params.cfp(i); -params.cfp(i); -params.sfp(i); params.sfp(i)];
                dyvl{i} = [params.sfp(i); -params.sfp(i); params.cfp(i); -params.cfp(i)];
            end
            
            bvl = [vxmax; vxmax; vymax; vymax];
        end
        
        function [dxa, dya, ba] = makeAccelerationLimits(params, axmax, aymax)
            dxa = cell(1, params.Nr);
            dya = cell(1, params.Nr);
            
            for i=1:params.Nr
                dxa{i} = [params.cfp(i); -params.cfp(i); -params.sfp(i); params.sfp(i)];
                dya{i} = [params.sfp(i); -params.sfp(i); params.cfp(i); -params.cfp(i)];
            end
            
            ba = [axmax; axmax; aymax; aymax];
        end
        
        function [dxsp, dysp, bsp] = makeSupportPolygon(params, footLength, footWidth)
            dxsp = cell(1, params.Nr);
            dysp = cell(1, params.Nr);
            
            for i=1:params.Nr
                dxsp{i} = [params.cfp(i); -params.cfp(i); -params.sfp(i); params.sfp(i)];
                dysp{i} = [params.sfp(i); -params.sfp(i); params.cfp(i); -params.cfp(i)];
            end
            
            l = footLength;
            w = footWidth;
            bsp = [l/2; l/2; w/2; w/2];
        end
        
        function constraints = makeIntegerConstraints(variables)
            Nf = variables.Nf;
            N = variables.N;
            o = Nf + 10; % To make sure we aren't going to have numeric problems
            constraints = [variables.Ub(1, 1) == 1];
            for i=1:Nf-1
                constraints = constraints +...
                    [(1:i+1) * variables.Ub(i+1, 1:i+1)' - (1:i) * variables.Ub(i, 1:i)' >= 0];
                constraints = constraints +...
                    [(o+1:o+i+1) * variables.Ub(i+1, 1:i+1)' - (o+1:o+i) * variables.Ub(i, 1:i)' <= 1];
            end
            for i=Nf:N-1
                constraints = constraints +...
                    [(1:Nf) * variables.Ub(i+1, 1:Nf)' - (1:Nf) * variables.Ub(i, 1:Nf)' >= 0];
                constraints = constraints +...
                    [(o+1:o+Nf) * variables.Ub(i+1, 1:Nf)' - (o+1:o+Nf) * variables.Ub(i, 1:Nf)' <= 1];
            end
        end
        
        function constraints = makeAuxiliaryVariablesConstraints(variables)
            N = variables.N;
            Nf = variables.Nf;
            Nr = variables.Nr;
            constraints = [];
            for j=1:Nf
                for i=j:N
                    constraints = constraints +...
                        implies(variables.Ub(i, j), variables.Xfaux(i) == variables.Xfb(j));
                    constraints = constraints +...
                        implies(variables.Ub(i, j), variables.Yfaux(i) == variables.Yfb(j));
                    constraints = constraints +...
                        implies(variables.Ub(i, j), (1:Nr) * variables.Thetafaux(i, :)' == (1:Nr) * variables.Thetafb(j, :)');
                end
            end
        end
        
        function constraints = makeZMPErrorConstraints(variables, params, prediction,...
                dxsp, dysp, bsp)
            N = variables.N;
            Nr = variables.Nr;
            constraints = [];
            
            Zx = prediction.Pzs * params.x + prediction.Pzu * variables.d3X;
            Zy = prediction.Pzs * params.y + prediction.Pzu * variables.d3Y;
            for i=1:N
                for m=1:Nr
                    constraints = constraints +...
                        implies(variables.Thetafaux(i, m),...
                        [dxsp{m}, dysp{m}] * [Zx(i) - variables.Xfaux(i); Zy(i) - variables.Yfaux(i)] <= bsp);
                end
            end
        end
        
        function constraints = makeFeetCollisionConstraints(variables, params,...
                dxfr, dyfr, bfr)
            Nf = variables.Nf;
            Nr = variables.Nr;
            constraints = [];
            for j=1:Nf-1
                factor = (1 - 2 * params.bls) * (-1)^(j - 1); % had to compute without exp due to YALMIP optimizer limitations
                for m=1:Nr
                    constraints = constraints +...
                        implies(variables.Thetafb(j, m),...
                        [factor * [dxfr{m}, dyfr{m}] * [variables.Xfb(j+1) - variables.Xfb(j); variables.Yfb(j+1) - variables.Yfb(j)] <= bfr]);
                end
                constraints = constraints +...
                    [factor * (-1)^(j - 1) * (params.Thetafp' * variables.Thetafb(j+1, :)' - params.Thetafp' * variables.Thetafb(j, :)') <= 0.0];
            end
        end
        
        function constraints = makeJointsConstraints(variables, params,...
                dxvl, dyvl, bvl, T, dthetaMax)
            N = variables.N;
            Nf = variables.Nf;
            Nr = variables.Nr;
            for j=1:Nf
                S(j, 1) = sum(variables.Ub(j:N, j));
            end
            constraints = [[dxvl{1}, dyvl{1}] * [variables.Xfb(2) - params.xa; variables.Yfb(2) - params.ya] <= (S(1) - params.bds) * T * bvl];
            for j=2:Nf-1
                for m=1:Nr
                    constraints = constraints +...
                        implies(variables.Thetafb(m) + variables.Bs(j) == 2,...
                        [[dxvl{m}, dyvl{m}] * [variables.Xfb(j+1) - variables.Xfb(j-1); variables.Yfb(j+1) - variables.Yfb(j-1)] <= (S(j) - 1) * T * bvl]);
                end
            end
            constraints = constraints +...
                [-(S(1, 1) - params.bds) * T * dthetaMax <= params.Thetafp' * variables.Thetafb(2, :)' - params.thetaa <= (S(1, 1) - params.bds) * T * dthetaMax];
            for j=2:Nf-1
                constraints = constraints +...
                    implies(variables.Bs(j), -(S(j, 1) - 1) * T * dthetaMax <= params.Thetafp' * variables.Thetafb(j+1, :)' - params.Thetafp' * variables.Thetafb(j-1, :)' <= (S(j, 1) - 1) * T * dthetaMax);
            end
        end
        
        function constraints = makeCurrentStepConstraints(variables, params)
            constraints = [variables.Xfb(1) == params.xc, variables.Yfb(1) == params.yc, variables.Thetafb(1, 1) == 1];
        end
        
        function constraints = makeAccelerationConstraints(variables, params, prediction,...
                dxa, dya, ba)
            N = variables.N;
            Nr = variables.Nr;
            d2X = prediction.Pas * params.x + prediction.Pau * variables.d3X;
            d2Y = prediction.Pas * params.y + prediction.Pau * variables.d3Y;
            constraints = [];
            for i=1:N
                for m=1:Nr
                    constraints = constraints +...
                        implies(variables.Thetafaux(i, m),...
                        [dxa{m}, dya{m}] * [d2X(i); d2Y(i)] <= ba);
                end
            end
        end
        
        function constraints = makeStepDurationReferenceContinuousConstraints(variables,...
                params, nref)
            N = variables.N;
            Nf = variables.Nf;
            
            nref1 = max(nref - params.kprime, 0);
            nrefVector = [nref1; nref * ones(Nf-1, 1)];
            constraints = [variables.Sr(1) == nrefVector(1)];
            for j=2:Nf
                constraints = constraints +...
                    implies(variables.Bs(j),...
                    [variables.Sr(j) == nrefVector(j)]);
                sigma = sum(variables.Ub(:, 1));
                for k=2:j-1
                    sigma = sigma + sum(variables.Ub(k:end, k));
                end
                constraints = constraints +...
                    implies(~variables.Bs(j),...
                    [variables.Sr(j) == N - sigma]);
            end
        end
        
        function constraints = makeStepDurationReferenceBinaryConstraints(variables,...
                params, nref)
            N = variables.N;
            Nf = variables.Nf;
            
            nref1 = max(nref - params.kprime, 0);
            nrefVector = [nref1; nref * ones(Nf-1, 1)];
            constraints = [variables.Bs(1) == 1];
            for j=2:Nf
                sigma = sum(variables.Ub(:, 1));
                for k=2:j-1
                    sigma = sigma + sum(variables.Ub(k:end, k));
                end
                constraints = constraints +...
                    [(N - nrefVector(j)) * variables.Bs(j) >= N - sigma - nrefVector(j),...
                        nrefVector(j) * variables.Bs(j) <= N - sigma];
            end
        end
        
        function constraints = makeFootRotationConstraints(variables, params, nref, T)
            Nf = variables.Nf;
            constraints = [variables.deltaThetar(1) == 0];
            nref1 = max(nref - params.kprime, 0);
            nrefVector = [nref1; nref * ones(Nf-1, 1)];
            for j=1:Nf
                constraints = constraints + [sum(variables.Thetafb(j, :)) == 1];
            end
            for i=1:variables.N
                constraints = constraints + [sum(variables.Thetafaux(i, :)) == 1];
            end
            variables.Thetar(1, 1) = params.thetac;
            for j=2:Nf
                constraints = constraints +...
                    implies(sum(variables.Ub(:, j)) >= 1, variables.Thetar(j, 1) == variables.Thetar(j-1, 1) + variables.deltaThetar(j, 1));
                constraints = constraints +...
                    implies(sum(variables.Ub(:, j)) == 0, variables.Thetar(j, 1) == params.Thetafp' * variables.Thetafb(j, :)');
            end
            term = (1 - 2 * params.bls) * (1 - 2 * params.bccw); % had to compute without exp due to YALMIP optimizer limitations
            constraints = constraints +...
                implies(variables.Bs(2), variables.deltaThetar(2, 1) == ((1 + term) / 2) * (params.kprime + sum(variables.Ub(:, 1)) + sum(variables.Ub(:, 2))) * T * params.dthetar);
            constraints = constraints +...
                implies(~variables.Bs(2), variables.deltaThetar(2, 1) == ((1 + term) / 2) * (params.kprime + sum(variables.Ub(:, 1)) + nrefVector(2)) * T * params.dthetar);
            for j=3:Nf
                term = (1 - 2 * params.bls) * (1 - 2 * params.bccw) * (-1)^j;
                constraints = constraints +...
                    implies(variables.Bs(j), variables.deltaThetar(j, 1) == ((1 + term) / 2) * (sum(variables.Ub(:, j-1)) + sum(variables.Ub(:, j))) * T * params.dthetar);
                constraints = constraints +...
                    implies(~variables.Bs(j), variables.deltaThetar(j, 1) == ((1 + term) / 2) * (sum(variables.Ub(:, j-1)) + nrefVector(j)) * T * params.dthetar);
            end
        end
        
        function constraints = makeBoundConstraints(variables, params)
            N = variables.N;
            Nf = variables.Nf;
            Nr = variables.Nr;
            constraints = [-1000 * ones(N, 1) <= variables.d3X <= 1000 * ones(N, 1),...
                -10 * ones(Nf, 1) <= variables.Xfb <= 10 * ones(Nf, 1),...
                -1000 * ones(N, 1) <= variables.d3Y <= 1000 * ones(N, 1),...
                -10 * ones(Nf, 1) <= variables.Yfb <= 10 * ones(Nf, 1),...
                -10 * ones(N, 1) <= variables.Xfaux <= 10 * ones(N, 1),...
                -10 * ones(N, 1) <= variables.Yfaux <= 10 * ones(N, 1),...
                zeros(Nf, 1) <= variables.Sr <= N * ones(Nf, 1),...
                -10 * ones(Nf, 1) <= variables.Thetar <= 10 * ones(Nf, 1),...
                -10 * ones(Nf, 1) <= variables.deltaThetar <= 10 * ones(Nf, 1)];
            constraints = constraints +...
                [-1 * ones(N, 1) <= params.dXr <= 1 * ones(N, 1),...
                -1 * ones(N, 1) <= params.dYr <= 1 * ones(N, 1),...
                -5 * pi <= params.dthetar <= 5 * pi,...
                0 <= params.kprime <= N,...
                -100 * ones(3, 1) <= params.x <= 100 * ones(3, 1),...
                -100 * ones(3, 1) <= params.y <= 100 * ones(3, 1),...
                -10 <= params.xc <= 10,...
                -10 <= params.yc <= 10,...
                -5 * pi <= params.thetac <= 5 * pi,...
                -10 <= params.xa <= 10,...
                -10 <= params.ya <= 10,...
                -5 * pi <= params.thetaa <= 5 * pi,...
                0 <= params.bls <= 1,...
                0 <= params.bccw <= 1,...
                -5 * pi * ones(Nr, 1) <= params.Thetafp <= 5 * pi * ones(Nr, 1),...
                -1 * ones(Nr, 1) <= params.cfp <= 1 * ones(Nr, 1),...
                -1 * ones(Nr, 1) <= params.sfp <= 1 * ones(Nr, 1)];
        end
        
        function constraints = makeBoundConstraintsOnlyVariables(variables)
            N = variables.N;
            Nf = variables.Nf;
            constraints = [-1000 * ones(N, 1) <= variables.d3X <= 1000 * ones(N, 1),...
                -10 * ones(Nf, 1) <= variables.Xfb <= 10 * ones(Nf, 1),...
                -1000 * ones(N, 1) <= variables.d3Y <= 1000 * ones(N, 1),...
                -10 * ones(Nf, 1) <= variables.Yfb <= 10 * ones(Nf, 1),...
                -10 * ones(N, 1) <= variables.Xfaux <= 10 * ones(N, 1),...
                -10 * ones(N, 1) <= variables.Yfaux <= 10 * ones(N, 1),...
                zeros(Nf, 1) <= variables.Sr <= N * ones(Nf, 1),...
                -10 * ones(Nf, 1) <= variables.Thetar <= 10 * ones(Nf, 1),...
                -10 * ones(Nf, 1) <= variables.deltaThetar <= 10 * ones(Nf, 1)];
        end
    end
end