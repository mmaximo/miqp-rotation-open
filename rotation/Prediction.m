classdef Prediction < handle
    properties
        Pps;
        Ppu;
        Pvs;
        Pvu;
        Pas;
        Pau;
        Pzs;
        Pzu;
    end
    
    methods
        function self = Prediction(dynamics, N)
            n = size(dynamics.A, 1);
            p = size(dynamics.B, 2);
            self.Pps = zeros(N, n);
            self.Ppu = zeros(N, N * p);
            self.Pvs = zeros(N, n);
            self.Pvu = zeros(N, N * p);
            self.Pas = zeros(N, n);
            self.Pau = zeros(N, N * p);
            self.Pzs = zeros(N, n);
            self.Pzu = zeros(N, N * p);
            T = dynamics.T;
            zCOM = dynamics.zCOM;
            g = Constants.GRAVITY;
            for i=1:N
                self.Pps(i, :) = [1, i * T, i^2* T^2 / 2];
                self.Pvs(i, :) = [0, 1, i * T];
                self.Pas(i, :) = [0, 0, 1];
                self.Pzs(i, :) = [1, i * T, i^2 * T^2 / 2 - zCOM / g];
            end
            for i=1:N
                for j=1:i
                    factor = 1 + 3 * (i - j) + 3 * (i - j)^2;
                    self.Ppu(i, j) = factor * T^3 / 6;
                    self.Pvu(i, j) = (1 + 2 * (i - j)) * T^2 / 2;
                    self.Pau(i, j) = T;
                    self.Pzu(i, j) = factor * T^3 / 6 - T * zCOM / g;
                end
            end
        end
        
        function [X, Z] = predictFuture(self, x, u)
            X = self.Pps * x + self.Ppu * u;
            Z = self.Pzs * x + self.Pzu * u;
        end
        
        function dX = predictVelocity(self, x, u)
            dX = self.Pvs * x + self.Pvu * u;
        end
        
        function d2X = predictAcceleration(self, x, u)
            d2X = self.Pas * x + self.Pau * u;
        end
    end
end