classdef SimulationRotation < handle
    properties
        dynamics;
        controller;
        state;
        Ncon;
        kc;
        kd;
        xc;
        yc;
        thetac;
        xa;
        ya;
        isLeftSwing;
        d3x;
        d3y;
        xf1;
        yf1;
        theta1;
        dxa;
        dya;
        thetaa;
        dthetaa;
        isSwitchingStep;
        Ntouch;
        Xh;
        Yh;
        Xch;
        Ych;
        Thetach;
        Zxh;
        Zyh;
        d3Xh;
        d3Yh;
        dXh;
        dYh;
        d2Xh;
        d2Yh;
        xah;
        yah;
        thetaah;
        Xf1h;
        Yf1h;
        changingh;
        numFutureSteps;
        numFutureStepsh;
        feasible;
    end
    
    methods
        function self = SimulationRotation(dynamics, controller,...
            params)
            self.dynamics = dynamics;
            self.controller = controller;
            self.state = params.state0;
            self.xc = params.xc;
            self.yc = params.yc;
            self.thetac = params.thetac;
            self.xa = params.xa;
            self.ya = params.ya;
            self.thetaa = params.thetaa;
            self.isLeftSwing = params.isLeftSwing;
            self.Ncon = int32(self.controller.T / self.dynamics.T);
            self.kc = 0;
            self.kd = 1;
            self.isSwitchingStep = false;
            self.feasible = true;
        end
        
        function simulate(self, Nd, dxref, dyref, dthetaref, disturbances)
            self.Xh = zeros(Nd, 1);
            self.Yh = zeros(Nd, 1);
            self.Xch = zeros(Nd, 1);
            self.Ych = zeros(Nd, 1);
            self.Zxh = zeros(Nd, 1);
            self.Zyh = zeros(Nd, 1);
            self.Thetach = zeros(Nd, 1);
            self.xah = zeros(Nd, 1);
            self.yah = zeros(Nd, 1);
            self.d3Xh = zeros(Nd, 1);
            self.d3Yh = zeros(Nd, 1);
            self.dXh = zeros(Nd, 1);
            self.dYh = zeros(Nd, 1);
            self.d2Xh = zeros(Nd, 1);
            self.d2Yh = zeros(Nd, 1);
            self.xah = zeros(Nd, 1);
            self.yah = zeros(Nd, 1);
            self.thetaah = zeros(Nd, 1);
            self.Xf1h = zeros(Nd, 1);
            self.Yf1h = zeros(Nd, 1);
            self.changingh = zeros(Nd, 1);
            
            xDist = zeros(3, Nd);
            yDist = zeros(3, Nd);
            for d=1:length(disturbances)
                kDist = disturbances(d).kd;
                xDist(:, kDist) = disturbances(d).xDist;
                yDist(:, kDist) = disturbances(d).yDist;
            end
            
            for k=1:Nd
                if ~self.feasible
                    break;
                end
                self.step(dxref, dyref, dthetaref, xDist(:, k), yDist(:, k));
                
                x = self.state(1:3);
                y = self.state(4:6);
                self.Xh(k) = x(1);
                self.Yh(k) = y(1);
                self.Xch(k) = self.xc;
                self.Ych(k) = self.yc;
                self.Zxh(k) = self.dynamics.output(x);
                self.Zyh(k) = self.dynamics.output(y);
                self.Thetach(k) = self.thetac;
                self.d3Xh(k) = self.d3x;
                self.d3Yh(k) = self.d3y;
                self.dXh(k) = x(2);
                self.dYh(k) = y(2);
                self.d2Xh(k) = x(3);
                self.d2Yh(k) = y(3);
                self.numFutureStepsh(k) = self.numFutureSteps;
                self.xah(k) = self.xa;
                self.yah(k) = self.ya;
                self.thetaah(k) = self.thetaa;
                self.Xf1h(k) = self.xf1;
                self.Yf1h(k) = self.yf1;
                self.changingh(k) = self.isSwitchingStep;
            end
        end
        
        function step(self, dxref, dyref, dthetaref, xDist, yDist)
            rotatedDistX = cos(self.thetac) * xDist - sin(self.thetac) * yDist;
            rotatedDistY = sin(self.thetac) * xDist + cos(self.thetac) * yDist;
%             self.state = self.state + [xDist; yDist];
            self.state = self.state + [rotatedDistX; rotatedDistY];
            x = self.state(1:3);
            y = self.state(4:6);
            
            if mod(self.kd, self.Ncon) == 1
                if self.isSwitchingStep == true
                    self.isSwitchingStep = false;
                end
                if self.Ntouch == 1
                    self.switchStep();
                end
%                 if self.isSwitchingStep == true
%                     self.switchStep();
%                 end
                dXref = dxref * cos(self.thetac) * ones(self.controller.N, 1) - dyref * sin(self.thetac) * ones(self.controller.N, 1);
                dYref = dxref * sin(self.thetac) * ones(self.controller.N, 1) + dyref * cos(self.thetac) * ones(self.controller.N, 1);
                thetaSatisfied = false;
                dthetarefaux = dthetaref;
%                 while thetaSatisfied == false
                    dThetaref = self.planTheta(dthetarefaux);
%                     dThetaref
                    theta = [self.thetac; self.thetac + dThetaref];

                    if strcmp(self.controller.type, 'humanoids')
                        [d3X, d3Y, Xfb, Yfb, Xfaux, Yfaux, Bu] =...
                            self.controller.control(self.kc, x, y, dXref, dYref,...
                            self.xc, self.yc, self.xa, self.ya, theta, self.isLeftSwing);
                        self.Ntouch = self.computeNtouch(Bu);
                    elseif strcmp(self.controller.type, 'optimizer')
                        [d3X, d3Y, Xfb, Yfb, Xfaux, Yfaux, Thetafb, Thetafaux, Bu, self.feasible] =...
                            self.controller.control(self.kc, x, y, dXref, dYref, dthetaref,...
                            self.xc, self.yc, self.xa, self.ya, self.thetaa, theta, self.isLeftSwing);
                        self.Ntouch = self.computeNtouch(Bu);
                        deltaTheta = dthetaref * self.controller.T;
                        theta = self.thetac + deltaTheta * (0:self.controller.Nr-1) * Thetafb';
                        if ~self.feasible
                            return;
                        end
                    elseif strcmp(self.controller.type, 'rotation')
                        deltaTheta = dthetaref * self.controller.T;
                        [d3X, d3Y, Xfb, Yfb, Xfaux, Yfaux, Thetafb, Thetafaux, Bu] =...
                            self.controller.control(self.kc, x, y, dXref, dYref, dthetaref,...
                            self.xc, self.yc, self.xa, self.ya, self.thetaa, theta, self.isLeftSwing);
                        self.Ntouch = self.computeNtouch(Bu);
                        theta = self.thetac + deltaTheta * (0:self.controller.Nr-1) * Thetafb';
                    elseif strcmp(self.controller.type, 'first_miqp')
                        [d3X, d3Y, Xfb, Yfb, Xfaux, Yfaux, B] =...
                            self.controller.control(self.kc, x, y, dXref, dYref,...
                                self.xc, self.yc, self.xa, self.ya, theta, self.isLeftSwing);
                        self.Ntouch = (1:self.controller.N) * B;
                    elseif strcmp(self.controller.type, 'first_qp')
                        [d3X, d3Y, Xfb, Yfb, S1] =...
                            self.controller.control(self.kc, x, y, dXref, dYref,...
                                self.xc, self.yc, self.xa, self.ya, theta, self.isLeftSwing);
                        self.Ntouch = S1;
                    end
                    
                    maxDthetaPerTimestep = 100000;
%                     maxDthetaPerTimestep = 1;
%                     maxDthetaPerTimestep = 0.075;
%                     if abs((theta(2) - self.thetaa) / self.Ntouch) > 1.1 * maxDthetaPerTimestep
%                         disp('Exceed rotation limit!');
%                         if sign(dthetarefaux) > 0.5
%                             dthetarefaux = maxDthetaPerTimestep * self.Ntouch + (self.thetaa - self.thetac);
%                         else
%                             dthetarefaux = -maxDthetaPerTimestep * self.Ntouch - (self.thetaa - self.thetac);
%                         end
%                     else
%                         thetaSatisfied = true;
%                     end
%                     
%                 self.Ntouch = self.computeNtouch(Bu);
%                 self.Ntouch
%                 if self.Ntouch == 1
%                     self.isSwitchingStep = true;
%                 end
                    self.numFutureSteps = self.computeNumFutureSteps(Bu);
                    self.d3x = d3X(1);
                    self.d3y = d3Y(1);
                    self.xf1 = Xfb(2);
                    self.yf1 = Yfb(2);
                    self.theta1 = theta(2);
                    self.dxa = (self.xf1 - self.xa) / double(self.Ncon * self.Ntouch);
                    self.dya = (self.yf1 - self.ya) / double(self.Ncon * self.Ntouch);
                    self.dthetaa = (self.theta1 - self.thetaa) / double(self.Ncon * self.Ntouch);
%                 if self.dthetaa > 0.1
%                     self.dthetaa = 0.1;
%                 end

%                 end
            end
            
            x = self.dynamics.step(x, self.d3x);
            y = self.dynamics.step(y, self.d3y);
            
            self.state = [x; y];
            
            self.kd = self.kd + 1;
            if mod(self.kd, self.Ncon) == 1
                self.kc = self.kc + 1;
            end
            if ~self.isSwitchingStep
                self.xa = self.xa + self.dxa;
                self.ya = self.ya + self.dya;
                self.thetaa = self.thetaa + self.dthetaa;
            end
        end
        
        function switchStep(self)            
            self.kc = 0;
            self.isSwitchingStep = true;
            auxX = self.xa;
            auxY = self.ya;
            auxTheta = self.thetaa;
            self.xa = self.xc;
            self.ya = self.yc;
            self.thetaa = self.thetac;
            self.xc = auxX;
            self.yc = auxY;
            self.thetac = auxTheta;
%             self.xc = self.xf1;
%             self.yc = self.yf1;
%             self.thetac = self.theta1;
            self.isLeftSwing = ~(self.isLeftSwing);
        end
        
        function dTheta = planTheta(self, dtheta)
            k = self.kc;
            Tstep = self.controller.T * self.controller.Nstep;
%             k = mod(self.kc - 1, self.controller.Nstep) + 1;
            numSteps = self.controller.getNumSteps(k);
            dTheta = zeros(numSteps, 1);
            ils = self.isLeftSwing;
            thetaAcc = 0;
            for i=1:numSteps
                if dtheta > 0 && ils ||...
                        dtheta < 0 && ~ils
                    thetaAcc = thetaAcc + 2 * dtheta * Tstep;
                end
                dTheta(i) = thetaAcc;
                ils = ~ils;
            end
        end
        
        function Ntouch = computeNtouch(self, B)
           Ntouch = 0;
           N = self.controller.N;
           Nf = self.controller.Nf;
           for i=1:Nf
               if B(i*(i-1)/2+1) > 10^-3
                   Ntouch = Ntouch + 1;
               end
           end
           for i=Nf+1:N
               if B(Nf*(Nf+1)/2+(i-Nf-1)*Nf+1) > 10^-3
                   Ntouch = Ntouch + 1;
               end
           end
        end
      
       
        function numFutureSteps = computeNumFutureSteps(self, B)
            binaryMatrix = self.makeBinaryMatrix(B);
            numColumns = size(binaryMatrix, 2);
            sumRows = sum(binaryMatrix);
            numFutureSteps = 0;
            for j=1:numColumns
               if sumRows(j) > 10^-3
                   numFutureSteps = numFutureSteps + 1;
               end
            end
        end
        
        function binaryMatrix = makeBinaryMatrix(self, B)
           binaryMatrix = zeros(self.controller.N, self.controller.Nf);
           N = self.controller.N;
           Nf = self.controller.Nf;
           for i=1:Nf
               for j=1:i
                   binaryMatrix(i, j) = B(i*(i-1)/2+j);
               end
           end
           for i=Nf+1:N
               for j=1:Nf
                   binaryMatrix(i, j) = B(Nf*(Nf+1)/2+(i-Nf-1)*Nf+j);
               end
           end
       end
    end
end