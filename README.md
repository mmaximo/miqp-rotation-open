# MIQP Rotation Open


- Download the lastest version of YALMIP from https://yalmip.github.io/download/ and put the folder "YALMIP-master" in the root directory of the repository.
- Download and install the latest version of Gurobi from https://www.gurobi.com/
- The code is folder "rotation". You first need to run the script configurePath.m to load YALMIP.
- You need to load Gurobi. The script loadGurobi.m does that for Gurobi 9.0.0. Please, adapt the script if you use another version of Gurobi.
- The main script is runSimulationRotation.m.